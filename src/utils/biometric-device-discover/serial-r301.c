/*
 * Copyright (C) 2018 Tianjin KYLIN Information Technology Co., Ltd.
 *
 * Author: Droiing <jianglinxuan@kylinos.cn>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <dlfcn.h>
#include <unistd.h>
#include <math.h>
#include <sys/fcntl.h>
#include <arpa/inet.h>
#include <time.h>
#include <dirent.h>

#include <biometric_common.h>
#include <biometric_intl.h>

#include "serial-r301.h"

#define READ_TIMEOUT_MS 100

int R301SendData(int fd, uint8 *data, int count)
{
	int ret = 0;

	if (fd < 0){
		bio_print_debug(_("No R301 finger print device detected\n"));
		return -1;
	}

	ret = write(fd, data, count);

	return ret;
}

int R301RecvData(int fd, uint8 *data, int count)
{
	int ret = 0;
	int len = 0;

	if (fd < 0){
		bio_print_debug(_("No R301 finger print device detected\n"));
		return -1;
	}

	struct timeval start, now;
	gettimeofday(&start, NULL);
	gettimeofday(&now, NULL);
	int elapsed_ms = 0;
	while (elapsed_ms < READ_TIMEOUT_MS && len < count)
	{
		usleep(1000);
		ret = read(fd, data + len, count - len);
		len = len + ret;
		gettimeofday(&now, NULL);
		elapsed_ms = (now.tv_sec - start.tv_sec) * 1000
					 + (now.tv_usec - start.tv_usec) / 1000;
	}

	return len;
}

static uint16 R301ChecksumGen(uint8 *data,int length)
{
	uint16	checksum = 0;
	int i;

	for (i=0;i<length;i++ )
		checksum += data[i];

	return checksum;
}

static int R301RespondParse(int length)
{
	int ret = 0;
	uint16 checksum = 0;
	RespondPackage *Respond_p = (RespondPackage *)PackageBuffer;

	if (Respond_p->head.id != BagID_Res)
	{
		bio_print_debug(_("R301RespondParse: Not a respond package\n"));
		ret = -1;
		goto out;
	}

	if (ntohs(Respond_p->head.length) != (length - sizeof(PackageHeader)))
	{
		bio_print_debug(_("R301RespondParse: Respond package length error\n"));
		ret = -1;
		goto out;
	}

	checksum = R301ChecksumGen(&PackageBuffer[6],length - 8);

	/*接收应答*/
	if ((PackageBuffer[length -2] != ((checksum >> 8)&0xff)) ||
				(PackageBuffer[length -1] != (checksum&0xff)))
	{
		bio_print_debug(_("R301RespondParse: Respond package checksum error\n"));
		ret = -1;
		goto out;
	}

//	bio_print_debug("respond code = %d\n", Respond_p->code);
	ret = Respond_p->code;
out:
	return ret;
}

/************************************************************************
函数名称： R301SendDataPackage
功能说明： 发送数据包
传入参数： data(要发送的数据)
		   length(数据长度)
输出参数： int（成功为0，失败为非0）
************************************************************************/
int R301SendDataPackage(int fd, uint8 * data, int length)
{
		int ret;
		DataPackage *data_p = (DataPackage *)PackageBuffer;
		uint16 checksum;

	/*封装数据包*/
	data_p->head.prefix = htons(FramePrefix);
	data_p->head.addr = htonl(addr);
	data_p->head.id = BagID_Data;
	data_p->head.length = htons(CheckSumSize + length);

	if (data != NULL)
	{
		memcpy(data_p->data, data, length);
	}

	/*计算校验和*/
	checksum = R301ChecksumGen(&PackageBuffer[6],length + 3);
	data_p->data[length] = (checksum >> 8)&0xff;
	data_p->data[length + 1] = checksum&0xff;

	/*发送数据包*/
	ret = R301SendData(fd, PackageBuffer,length+11);
	if (ret < length)
	{
		bio_print_debug(_("R301 send data package error\n"));
		ret = -1;
		goto out;
	}

	return 0;
out:
	return ret;
}

/************************************************************************
函数名称： SendCommand
功能说明： 发送指令包并接收应答包
传入参数： command(指令代码)
	   arg(输入参数，包含BufferID,StartPage,PageNum等)
	   arg_len(输入参数长度)
	   res(返回参数，包含PageID，MatchScore等)
	   respond_len(返回参数长度)
输出参数： int（成功为0，失败为非0）
************************************************************************/
int R301SendCommand(int fd, uint8 command, uint8 *arg, int arg_len,
					uint8 *res, int respond_len)
{
	int ret;
	int length;
	CommandPackage *command_p = (CommandPackage *)PackageBuffer;
	RespondPackage *respond_p = (RespondPackage *)PackageBuffer;
	uint16 checksum;


	/*封装命令*/
	command_p->head.prefix = htons(FramePrefix);
	command_p->head.addr = htonl(addr);
	command_p->head.id = BagID_CMD;
	command_p->head.length = htons(1 + CheckSumSize + arg_len);
	command_p->command = command;

	if (arg !=NULL)
	{
		memcpy(command_p->arg,arg,arg_len);
	}

	length = sizeof(PackageHeader) + 1 + CheckSumSize + arg_len;
	checksum = R301ChecksumGen(&PackageBuffer[6],length - 8);
	command_p->arg[arg_len] = (checksum >> 8)&0xff;
	command_p->arg[arg_len + 1] = checksum&0xff;

	/*发送命令*/
	ret = R301SendData(fd, PackageBuffer, length);
	if (ret < length)
	{
		bio_print_debug(_("R301 send command error\n"));
		ret = -1;
		goto out;
	}

	/*接收应答*/
	memset(PackageBuffer,0,48);

	length = sizeof(PackageHeader) + 1 + CheckSumSize + respond_len;
	ret = R301RecvData(fd, PackageBuffer, length);
	if (ret < length)
	{
		bio_print_debug(_("R301 receive respond error\n"));
		ret = -1;
		goto out;
	}

	ret = R301RespondParse(length);
	if (ret == 0)
	{
		if (res != NULL)
			memcpy(res, respond_p->arg, respond_len);
	}
	else
		goto out;

	return 0;
out:
	return ret;
}

/************************************************************************
函数名称： R301SendFinish
功能说明： 发送结束包
传入参数： data(结束包的数据内容)
		   length(结束包的数据长度)
输出参数： int（成功为0，失败为非0）
************************************************************************/
int R301SendFinish(int fd, uint8 *data, int length)
{
	int ret = 0;
	FinishPackage *Finish_p = (FinishPackage *)PackageBuffer;
	uint16 checksum = 0;

	/*封装命令*/
	Finish_p->head.prefix = htons(FramePrefix);
	Finish_p->head.addr = htonl(addr);
	Finish_p->head.id = BagID_Finish;
	Finish_p->head.length = htons(CheckSumSize + length);

	if (data != NULL){
		memcpy(Finish_p->data,data,length);
	}

	checksum = R301ChecksumGen(&PackageBuffer[6],length + 3);
	Finish_p->data[length] = (checksum >> 8)&0xff;
	Finish_p->data[length + 1] = checksum&0xff;

	/*发送命令*/
	ret = R301SendData(fd, PackageBuffer,length + 11);
	if (ret < length){
		bio_print_debug(_("R301 send command error\n"));
		ret = -1;
	}

	return ret;
}

/************************************************************************
函数名称： R301ReadSysPara
功能说明： 读指纹模块基本参数
传入参数： 无
输出参数： int（成功返回基本参数，失败返回-1）
************************************************************************/
int R301ReadSysPara(int fd, uint8 res[16])
{
	int ret = 0;

	ret = R301SendCommand(fd, CMD_ReadSysPara, NULL, 0, res, 16);
	if (ret != 0 ){
		bio_print_debug(_("Get basic parameters failed\n"));
		goto out;
	}

	return 0;

out:
	return -1;

}

static int R301DeviceSet(int fd, int Speed, int Bits, char Event, int Stop)
{
	struct termios newtio;

	if (tcgetattr(fd, &oldtio) != 0){
		bio_print_debug(_("Get serial attribute failed\n"));
		return -1;
	}

	bzero(&newtio,sizeof(newtio));
	newtio.c_cflag |= CLOCAL |CREAD;
	newtio.c_cflag &= ~CSIZE;

	switch (Bits) {
	case 7:
		newtio.c_cflag |= CS7;
		break;
	case 8:
		newtio.c_cflag |= CS8;
		break;
	}

	switch (Event) {
	case 'O':
		newtio.c_cflag |= PARENB;
		newtio.c_cflag |= PARODD;
		newtio.c_iflag |= (INPCK | ISTRIP);
		break;
	case 'E':
		newtio.c_iflag |= (INPCK |ISTRIP);
		newtio.c_cflag |= PARENB;
		newtio.c_cflag &= ~PARODD;
		break;
	case 'N':
		newtio.c_cflag &= ~PARENB;
		break;
	}

	switch (Speed) {
	case 2400:
		cfsetispeed(&newtio,B2400);
		cfsetospeed(&newtio,B2400);
		break;
	case 4800:
		cfsetispeed(&newtio,B4800);
		cfsetospeed(&newtio,B4800);
		break;
	case 9600:
		cfsetispeed(&newtio,B9600);
		cfsetospeed(&newtio,B9600);
		break;
	case 57600:
		cfsetispeed(&newtio,B57600);
		cfsetospeed(&newtio,B57600);
		break;
	case 115200:
		cfsetispeed(&newtio,B115200);
		cfsetospeed(&newtio,B115200);
		break;
	case 460800:
		cfsetispeed(&newtio,B460800);
		cfsetospeed(&newtio,B460800);
		break;
	default:
		cfsetispeed(&newtio,B9600);
		cfsetospeed(&newtio,B9600);
		break;
	}

	if (Stop == 1) {
		newtio.c_cflag &= ~CSTOPB;
	} else if (Stop ==2) {
		newtio.c_cflag |= CSTOPB;
	}

	newtio.c_cc[VTIME] = READ_TIMEOUT_MS / 100;
	newtio.c_cc[VMIN] = 0;

	tcflush(fd,TCIFLUSH);
	if ((tcsetattr(fd, TCSANOW, &newtio)) != 0)
	{
		bio_print_debug(_("Set serial failed\n"));
		return -1;
	}

	return 0;
}



int R301PortControl(int fd, uint8 control)
{
	int ret = 0;
	uint8 arg[1];

	arg[0] = control;
	ret = R301SendCommand(fd, CMD_PortControl, arg, 1, NULL, 0);
	if (ret != 0 ){
		bio_print_debug(_("Set port for R301 failed\n"));
		return -1;
	}

	return ret;
}

int R301Discover(const char * path, int Speed, int Bits, char Event, int Stop)
{
	int fd = 0;
	int ret = 0;

	ret = access(path, F_OK | R_OK | W_OK);
	if ( ret != 0)
	{
		bio_print_debug(_("Cannot operate device node: %s\n"), path);
		return -1;
	}

	/*打开指纹设备*/
	fd = open(path, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0){
		bio_print_debug(_("Open R301 device failed\n"));
		return -1;
	}

	ret = fcntl(fd, F_SETFL, 0);
	if(ret < 0){
		bio_print_debug(_("Set R301 port[%s] file descriptor flag failed\n"), path);
		close(fd);
		return -1;
	}

	ret = R301DeviceSet(fd, Speed, Bits, Event, Stop);
	if (ret < 0){
		bio_print_debug(_("Set R301 device failed\n"));
		tcflush(fd, TCIFLUSH);
		tcsetattr(fd, TCSANOW, &oldtio);
		close(fd);
		return -1;
	}

	uint8 para[16];
	ret = R301ReadSysPara(fd, para);
	if (ret != 0)
	{
		bio_print_debug(_("Get R301 device parameter failed\n"));
		tcflush(fd, TCIFLUSH);
		tcsetattr(fd, TCSANOW, &oldtio);
		close(fd);
		return -1;
	}


	R301PortControl(fd, Control_OFF);

	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &oldtio);

	close(fd);

	return 0;
}

int main()
{
	char dev_node[MAX_PATH_LEN] = {0};
	int count = 0;

	DIR *dirp;
	struct dirent *direntp;

	dirp = opendir("/dev/");
	if (dirp == NULL) {
		bio_print_debug("\"/dev/ is not exist!\n\"");
		return 0;
	}

	while (direntp = readdir(dirp)) {
		if (strncmp(direntp->d_name, "tty", 3) != 0)
			continue;

		if (strlen(direntp->d_name) <= 3)
			continue;

		memset(dev_node, 0, MAX_PATH_LEN);
		snprintf(dev_node, MAX_PATH_LEN, "/dev/%s", direntp->d_name);
		bio_print_debug("Test %s\n", dev_node);

		int ret;
		ret = R301Discover(dev_node, 57600, 8, 'N', 1);

		if (ret == 0)
		{
			printf("%s\n", dev_node);
			// bio_print_debug("[v] %s\n", dev_node_list[idx]);
			count++;
		} else
		{
			// bio_print_debug("[x] %s\n", dev_node_list[idx]);
		}
	}

	return count;
}



