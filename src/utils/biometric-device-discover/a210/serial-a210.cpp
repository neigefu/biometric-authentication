#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <glib.h>

#include <biometric_common.h>
#include <biometric_intl.h>

#include "serial-a210.h"
#include "ComHelper.h"
#include "liba210.h"
#include "Serial.h"

static int a210_enroll_id = -1;
static int a210_identify_id = -1;
char a210_notify_string[NOTIFY_STRING_MAX_LEN] = {0};

int getIDFromResultString(char * str)
{
	char id_string[USER_ID_LEN] = {0};

    char * p;

	const char * CHAR_STRING = "ID: ";
	unsigned int len = strlen(CHAR_STRING);

	p = strstr(str, CHAR_STRING);
	if (p == NULL)
		return -1;

	if (strlen(p) > len)
        strncpy(id_string, p + len, strlen(p + len));

	id_string[USER_ID_LEN -1] = 0;
	if (strlen(id_string) > 0)
		return atoi(id_string);
	else
		return -1;
}

void parseResultString(char * str)
{
	/* 包含“识别成功”字符串代表需要处理识别结果 */
	if (strstr(str, _("Identify success")) != NULL)
	{
		a210_identify_id = getIDFromResultString(str);

#ifdef A210_DEBUG
		bio_print_info(_("Identified ID: %d\n"), a210_identify_id);
#endif
	}

	/* 包含“注册成功”字符串代表需要处理录入结果 */
	if (strstr(str, _("Enroll success")) != NULL)
	{
		a210_enroll_id = getIDFromResultString(str);

#ifdef A210_DEBUG
		bio_print_info(_("Enrolled ID: %d\n"), a210_enroll_id);
#endif
	}

	/* 其他操作成功无需解析字段 */
}

void HandleRecvData(char cCmd, unsigned char* cData, int nDatalen)
{
#ifdef A210_DEBUG_LOW_LEVEL
	GString * print_list = g_string_new(NULL);
    g_string_append(print_list, "当前命令: 0x%X, 返回数据：", cCmd);

	int i = 0;
    for (i = 0; i < nDatalen; i++)
        g_string_append(print_list, "%02X", cData[i]);

	bio_print_info("%s\n", print_list->str);
	g_string_free(print_list, TRUE);
#endif
}

void HandleResult(char* cInfo, int ilen, int ops_ret)
{
	memset(a210_notify_string, 0, sizeof(a210_notify_string));

#ifdef A210_DEBUG
	bio_print_debug(_("A210OpsResult[%d]: %s\n"), ilen, cInfo);
#endif

	switch (ops_ret) {
	case COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING:
		break;
	case COM_OPS_RET_CHECKSUM_ERROR:
	case COM_OPS_RET_ERROR:
		break;
	case COM_OPS_RET_SUCCESS:
		parseResultString(cInfo);
		break;
	case COM_OPS_RET_FAIL_OR_NO_MATCH:
		break;
	default:
		sprintf(a210_notify_string, _("Device returns unknown data and "
									  "skips processing"));
	}
}

void HandleRecvInfo(char* cInfo, int ilen)
{
#ifdef A210_DEBUG_LOW_LEVEL
	GString * print_list = g_string_new(NULL);
	g_string_append(print_list, _("A210RecvInfo: "));

	int i = 0;
    for (i = 0; i < ilen; i++)
        g_string_append(print_list, "%02X ", cInfo[i]);
	bio_print_info("%s\n", print_list->str);
	g_string_free(print_list, TRUE);
#endif
}

void HandleSendInfo(char* cInfo, int ilen)
{
#ifdef A210_DEBUG_LOW_LEVEL
	if (NULL == cInfo)
	{
		return;
	}

	GString *print_list = g_string_new(NULL);
	g_string_append(print_list, _("A210SendInfo: "));

	int i = 0;
    for (i = 0; i < ilen; i++)
        g_string_append(print_list, "%02X ", cInfo[i]);

	bio_print_info("%s\n", print_list->str);
	g_string_free(print_list, TRUE);
#endif
}

int A210Discover(char *compath)
{
	int ret = 0;

	// 关闭Debug日志功能
	A210_Startlog(1);
	// 设备探测阶段，设置超时时间为1s，防止进程长时间卡住
	A210_SetTimeout(1);
	A210_Init(HandleSendInfo, HandleRecvInfo, HandleResult, HandleRecvData);
	ret = A210_OpenComPort(compath, 19200, 8, 1, 'N');
	if (ret < 0 )
		return -1;
	ret = A210_StartDev();
	if (ret < 0)
		return -1;
	A210_CloseComPort();
	// 恢复超时时间
	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	return 0;
}

int main()
{
    char dev_node[MAX_PATH_LEN] = {0};
    int count = 0;

    DIR *dirp;
    struct dirent *direntp;

    dirp = opendir("/dev/");
    if (dirp == NULL) {
        bio_print_debug("\"/dev/ is not exist!\n\"");
        return 0;
    }

    while (direntp = readdir(dirp)) {
        if (strncmp(direntp->d_name, "tty", 3) != 0) 
            continue;

        if (strlen(direntp->d_name) <= 3)
            continue;

        memset(dev_node, 0, MAX_PATH_LEN);
        snprintf(dev_node, MAX_PATH_LEN, "/dev/%s", direntp->d_name);
        bio_print_debug("Test %s\n", dev_node);

        int ret;
        ret = A210Discover(dev_node);

        if (ret == 0) {
            printf("%s\n", dev_node);
            count++;
        } else {

        }
    }

    return count;
}
