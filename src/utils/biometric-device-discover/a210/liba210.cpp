#include "liba210.h"
#include <biometric_common.h>
CComOperator ComOper;

void A210_Startlog(int iEnable)
{
    ComOper.Startlog(iEnable);
}

void A210_ClearLog()
{
    ComOper.Clearlog();
}

int A210_Init(P_FUNC_SENDINFO funSend,P_FUNC_RECVINFO funRecv,P_FUNC_RESULT funRes,P_FUNC_RECVDATA funData)
{
    return ComOper.InitCallBack(funSend,funRecv,funRes,funData);
}

int A210_OpenComPort(char* compath,int nBaud,int nDatabit,int nStopbit,char cParity)
{
    return ComOper.InitPort(compath,nBaud,nDatabit,nStopbit,cParity);
}

int A210_CloseComPort()
{
    return ComOper.closePort();
}

void A210_SetTimeout(int timeout)
{
    ComOper.SetTimeout(timeout);
}

void A210_SetStopFlag(bool flag)
{
    ComOper.SetStopFlag(flag);
}

bool A210_GetStopFlag()
{
    return ComOper.GetStopFlag();
}


//启动信号
int A210_StartDev()
{
    int ret = 0;

    ComOper.SetReadlen(17);
    ret = ComOper.SendDataSync(START_DEVICE, 0, 0);

    return ret;
}

//启动识别
//userId全为 NULL(全为0) 则表示1：N识别，userId为-1(全FF)表示循环识别，否则只对 userId 进行1:1识别
int A210_Identify(char* userId)
{

    unsigned char cInfoData[USER_ID_LEN]={0};

    if (NULL != userId)
    {

        if (strcmp(userId, "-1") == 0)
        {
            for (int i = 0; i < USER_ID_LEN; i++)
            {
                cInfoData[i] = 0xFF;
            }
        }
        else
        {
            int Idlen = strlen(userId);
            if (Idlen > USER_ID_LEN)
            {
                bio_print_error("userId length error!\n");
                return -1;
            }

            ChangeIdToChar(userId, cInfoData);
        }
    }

    return ComOper.SendDataSync(USER_IDENTIFY,cInfoData,USER_ID_LEN);
}

//启动注册
int A210_Enroll(char* userId)
{
    if(NULL == userId )
    {
        bio_print_error("userId can not empty!\n");
        return -1;
    }

    int Idlen = strlen(userId);
    if(Idlen > USER_ID_LEN)
    {
         bio_print_error("humanID length error!\n");
        return -1;
    }

    unsigned char chInfoData[USER_ID_LEN] = {0};
    ChangeIdToChar(userId,chInfoData);

    return ComOper.SendDataSync(USER_ENROLL, chInfoData, USER_ID_LEN);
}

//删除模板
//userId为24个0删除全部本地存储的虹膜信息，否则删除对应userId的虹膜信息
int A210_DeleteTemplate(char* userId)
{
    unsigned char cInfoData[USER_ID_LEN]={0};
    if (NULL != userId)
    {
        int Idlen = strlen(userId);
        if (Idlen > USER_ID_LEN)
        {
            bio_print_error( "userId length error!\n");
            return -1;
        }

        ChangeIdToChar(userId, cInfoData);
    }

    return ComOper.SendDataSync(USER_DELETE,cInfoData,USER_ID_LEN);
}

//获取本地用户
//传入24位userId返回当前用户，全0返回全部用户
int A210_GetUser(char* userId)
{
    unsigned char cInfoData[USER_ID_LEN]={0};
    if(NULL != userId)
    {
        int ilen = strlen(userId);
        if(ilen > USER_ID_LEN)
        {
            bio_print_error("userId length error!\n");
            return -1;
        }

        ChangeIdToChar(userId,cInfoData);
    }

    return ComOper.SendDataSync(READ_USER,cInfoData,USER_ID_LEN);
}

//取消当前动作
int A210_CancelOp()
{
    return ComOper.SendDataSync(CANCLE_OP,NULL,0);
}

//获取当前设备状态
int A210_GetDevStatus()
{
    unsigned char cInfoData = 0;
    return ComOper.SendDataSync(GET_STATUS,&cInfoData,0);
}

//获取模板
int A210_GetTemplate(char* userId)
{

    if(NULL == userId )
    {
        bio_print_error("userId can not empty!\n");
        return -1;
    }

    int Idlen = strlen(userId);
    if(Idlen > USER_ID_LEN)
    {
      bio_print_error("userId length error!\n");
      return -1;
    }

    unsigned char cInfoData[USER_ID_LEN]={0};
    ChangeIdToChar(userId,cInfoData);

    return ComOper.SendDataSync(GET_PATTERN,cInfoData,USER_ID_LEN);
}

//下发模板
int A210_SendTemplate(char* userId,unsigned char* iris,int nIrislen)
{

    if(NULL == userId || NULL == iris)
    {
        bio_print_error("userId and iris can not empty!\n");
        return -1;
    }

    int Idlen = strlen(userId);
    if(Idlen > USER_ID_LEN)
    {
      bio_print_error("userId length error!\n");
      return -1;
    }

    unsigned char chUserId[USER_ID_LEN] = {0};
    ChangeIdToChar(userId,chUserId);

    int irislen = nIrislen;
    int iDatalen = USER_ID_LEN + irislen;

    unsigned char* cInfoData = new unsigned char[iDatalen + 1];
    memset(cInfoData,0,iDatalen + 1);
    memcpy(cInfoData,chUserId,USER_ID_LEN);
    memcpy(cInfoData+USER_ID_LEN,iris,irislen);

    int iret =  ComOper.SendData(SEND_PATTERN,cInfoData,iDatalen);

    if(cInfoData)
    {
        delete[] cInfoData;
        cInfoData = NULL;
    }

    return iret;
}

//升级应用
int A210_UpdateApp(unsigned char* appData,int nApplen)
{

    if( NULL == appData || nApplen <= 0 )
    {
        bio_print_error("The appData can not empty!\n");
        return -1;
    }

    return ComOper.SendDataSync(APP_UPDATE,appData,nApplen);
}

//切换识别模式
int A210_SetMode(int imode)
{
    if( imode >2 || imode <1)
       return -1;

    unsigned char cInfoData = (unsigned char)(imode & 0xFF);

    return ComOper.SendDataSync(CARD_MODE,&cInfoData,1);
}

//读卡号
int A210_ReadCard()
{

    return ComOper.SendDataSync(GET_CARD_ID,NULL,0);
}

//捕捉图像
int A210_CaptureImage()
{
    ComOper.SetReadlen(525);
    return ComOper.SendDataSync(CAPTURE_IMG,NULL,0);
}

//下发配置信息
int A210_SendConfigs(int enOpening,int enSharpness,int enStrabismus,int idOpening,int idSharpness,int idStrabismus)
{
    if(enOpening < 40 || enOpening > 90)
      {
           bio_print_error("enOpening should be between 40 to 90\n");
           return -1;
      }

    if(enSharpness < 40 || enSharpness > 90)
      {
           bio_print_error("enSharpness should be between 40 to 90\n");
           return -1;
      }

    if(enStrabismus < 40 || enStrabismus > 90)
      {
           bio_print_error("enStrabismus should be between 40 to 90\n");
           return -1;
      }

    if(idOpening < 40 || idOpening > 90)
      {
           bio_print_error("idOpening should be between 40 to 90\n");
           return -1;
      }

    if(idSharpness < 40 || idSharpness > 90)
      {
           bio_print_error("idSharpness should be between 40 to 90\n");
           return -1;
      }

    if(idStrabismus < 40 || idStrabismus > 90)
      {
           bio_print_error("idStrabismus should be between 40 to 90\n");
           return -1;
      }

    unsigned char cInfoData[PARA_DATA_LEN] = {0};
    cInfoData[0] = (char)(enOpening & 0xFF);
    cInfoData[1] = (char)(enSharpness & 0xFF);
    cInfoData[2] = (char)(enStrabismus & 0xFF);
    cInfoData[3] = (char)(idOpening & 0xFF);
    cInfoData[4] = (char)(idSharpness & 0xFF);
    cInfoData[5] = (char)(idStrabismus & 0xFF);

    return ComOper.SendDataSync(ENROLL_PARAM,cInfoData,PARA_DATA_LEN);
}

//开始产生模板
int A210_CreateRuntimeTemp()
{
    return ComOper.SendDataSync(PRO_REALTIME_TEMP,NULL,0);
}

//获取实时模板
int A210_GetRunTimeTemp()
{
    return ComOper.SendDataSync(GET_REALTIME_TEMP,NULL,0);
}

//结束产生实时模板
int A210_StopRunTimeTemp()
{
    return ComOper.SendDataSync(END_REALTIME_TEMP,NULL,0);
}


