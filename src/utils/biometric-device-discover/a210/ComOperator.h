#pragma once
#include <string.h>
#include "Serial.h"
#include "ComHelper.h"
class CComOperator
{
public:
    CComOperator(void);
    ~CComOperator(void);
public:
    int InitPort(char *Dev,int speed,int databits,int stopbits,char parity);
    void SetTimeout(int timeout);
    void SetStopFlag(bool flag);
    bool GetStopFlag();
    int closePort();
    int InitCallBack(P_FUNC_SENDINFO funSend,P_FUNC_RECVINFO funRecv,P_FUNC_RESULT funRes,P_FUNC_RECVDATA funData);
    int SendData(char cCmd,unsigned char* cData,int nDatalen);
    int SendDataSync(char cCmd,unsigned char* cData,int nDatalen);
public:
    void Startlog(int iEnable = 0);
    void Clearlog();
protected:
    int RecvedCmdDataExtract(unsigned char *cRecData, int nDataLen);
    void SendOnePackData(char cCmd,unsigned char* cData,int nDatalen);
    void SendMulPackData();
    int  RecvMulData(unsigned char *cRecData, int nDataLen);
    void GetDataHandler(unsigned char* cData,int nDataLen);
    void SendAck();
public:
    void wrapSendInfo(unsigned char* cInfo,int nInfolen);
    void wrapRecvInfo(unsigned char* cInfo,int nInfolen);
    void wrapRecvData(char cCmd,unsigned char* cData,int nDatalen);
    void wrapResultInfo(char* sInfo, int ops_ret);
    int  VerifyCmdAndData(char cCmd,unsigned char* cData,int nDatalen);
public:
    void SetReadlen(int ilen);
//	static CSerial m_serial;
protected:
    void Writelog(char* slog);
    void Writelog(string strlog);
    string Getlocaltime();
    void GetCurUserId(char cCmd,unsigned char* data,int nDatalen);
    void GetUserList(unsigned char* cData,int nDatalen);
    void GetUserId(unsigned char* data,int nDatalen,string &sUserId);
private:
    CSerial m_serial;
    int     m_iEnablelog;
    bool    m_bPortOpend;
    string  m_sResult;
    string  m_strUserId;
    string  m_strUserList;
    char    m_curCmd;
    char    m_cUserid[USER_ID_LEN+1];
    int     m_curMode;//当前识别模式

    unsigned char        m_cRecvBuffer[DATA_BUFFER_SIZE];
    unsigned char*       m_pRecvMulData;

    int         m_nCurRecvDataCount;
    int         m_nSendDataCount;
    int         m_nRecvMullen;
    int         m_nBodyLen;//多包包个数

    int         m_nMaxPack;//总包数，下标从0开始
    int         m_nPackIndex;//当前包号，下标从0开始

    vector<BodyData> m_vecBody;

private:
        P_FUNC_SENDINFO m_pFuncSend;
    P_FUNC_RECVINFO m_pFuncRecv;
    P_FUNC_RESULT   m_pFuncResult;
    P_FUNC_RECVDATA m_pFuncData;

public:
    static int RecveData(CComOperator* pRhs,char* pRecvData,int nRecvLen);
};

enum ComOpsRet {
    COM_OPS_RET_SUCCESS = SERIAL_SUCCESS,
    COM_OPS_RET_ERROR = SERIAL_ERROR,
    COM_OPS_RET_STOP_BY_USER = SERIAL_STOP_BY_USER,
    COM_OPS_RET_TIMEOUT = SERIAL_TIMEOUT,

    COM_OPS_RET_FAIL_OR_NO_MATCH = SERIAL_MAX + 1,
    COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING,
    COM_OPS_RET_ILLEGAL_DATA_PACKAGE,
    COM_OPS_RET_CHECKSUM_ERROR,
    COM_OPS_RET_MAX,
};

#define COM_OPERATOR_DEBUG
