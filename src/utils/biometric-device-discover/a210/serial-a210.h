#ifndef SERIALA210_H
#define SERIALA210_H

#define A210_DEBUG
//#define A210_DEBUG_LOW_LEVEL

#define NOTIFY_STRING_MAX_LEN 255

void HandleSendInfo(char* cInfo, int ilen);

void HandleRecvInfo(char* cInfo, int ilen);

void HandleResult(char* cInfo, int ilen, int ops_ret);

void HandleRecvData(char cCmd, unsigned char* cData, int nDatalen);

void a210_set_ops_result_by_device_ops_ret(bio_dev * dev, int ops_type, int ret);

int A210Discover(bio_dev *dev, char *compath);

#endif // SERIALA210_H
