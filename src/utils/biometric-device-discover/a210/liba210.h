#pragma once
#include "ComOperator.h"
#include "ComHelper.h"

//启用日志
void A210_Startlog(int iEnable);
//清空日志
void A210_ClearLog();
//初始化回调函数
int A210_Init(P_FUNC_SENDINFO funSend,P_FUNC_RECVINFO funRecv,P_FUNC_RESULT funRes,P_FUNC_RECVDATA funData);
//打开串口
int A210_OpenComPort(char* compath,int nBaud = 115200,int nDatabit=8,int nStopbit=1,char cParity='N');
//关闭串口
int A210_CloseComPort();

//启动信号 0x41
int A210_StartDev();

//启动识别 0x42
int A210_Identify(char* userId);

//启动注册 0x43
int A210_Enroll(char* userId);

//删除模板 0x44
int A210_DeleteTemplate(char* userId);

//获取本地用户 0x45
int A210_GetUser(char* userId);

//取消当前动作 0x46
int A210_CancelOp();

//获取当前设备状态 0x47
int A210_GetDevStatus();

//获取模板 0x48
int A210_GetTemplate(char* userId);

//下发模板 0x49
int A210_SendTemplate(char* userId,unsigned char* iris,int nIrislen);

//升级应用 0x4A
int A210_UpdateApp(unsigned char* appData,int nApplen);

//切换识别模式 0x4B
int A210_SetMode(int imode);

//读卡号 0x4C
int A210_ReadCard();

//开始产生模板 0x4D
int A210_CreateRuntimeTemp();

//获取实时模板 0x4E
int A210_GetRunTimeTemp();

//捕捉图像 0x4F
int A210_CaptureImage();

//结束产生实时模板 0x51
int A210_StopRunTimeTemp();

//下发配置信息 0x55
int A210_SendConfigs(int enOpening,int enSharpness,int enStrabismus,int idOpening,int idSharpness,int idStrabismus);

void A210_SetTimeout(int timeout);
void A210_SetStopFlag(bool flag);
bool A210_GetStopFlag();

#define A210_USER_MAX	100

#ifndef LIBA210_H
#define LIBA210_H

#endif // LIBA210_H
