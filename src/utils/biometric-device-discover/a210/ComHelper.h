/***************************************************************************************************
文件名：ComHelper.h
文件说明：本文件为X1000模块的串口通信协议头文件，定义了串口协议的包结构，宏定义，操作函数定义等
适用协议版本：
软件修改历史：
    2016-12-19  徐顺杰

****************************************************************************************************/
#ifndef _R601COMHELPER_H_
#define _R601COMHELPER_H_
#pragma once
#include <vector>
#include <string>
#include <iomanip>
using namespace std;

#define START_DEVICE		'A' //0x41 启动信号
#define USER_IDENTIFY		'B' //0x42 启动识别
#define USER_ENROLL			'C' //0x43 启动注册
#define USER_DELETE			'D' //0x44 删除模板
#define READ_USER			'E' //0x45 获取本地用户
#define CANCLE_OP			'F' //0x46 取消当前动作
#define GET_STATUS			'G' //0x47 获取当前设备状态
#define GET_PATTERN			'H' //0x48 获取模板
#define SEND_PATTERN		'I' //0x49 下发模板
#define APP_UPDATE          'J' //0x4A 升级应用
#define CARD_MODE			'K' //0x4B 切换识别模式
#define GET_CARD_ID			'L' //0x4C 读卡号
#define CAPTURE_IMG         'O' //0x4F 提取图像
#define PACK_ACK		    'S' //0x53 应答握手包
#define BBC_ERROR			'T' //0x54 校验错误握手包
#define ENROLL_PARAM	    'U' //0x55 下发配置信息
#define ENROLL_FEEDBACK	    'V' //0x56 注册反馈
#define PRO_REALTIME_TEMP   'M' //0x4D
#define GET_REALTIME_TEMP   'N' //0x4E
#define END_REALTIME_TEMP   'Q' //0x51


#define CMD_TAGHEX			0x55
#define CMD_ETX				0x03
#define CMD_TYPE			0x01

#define TAGLEN		        3
#define NODATA_LEN          13
#define FRAME_NODATA_LEN    10
#define MAX_PACK_LEN		525
#define MAX_DATA_LEN	    512
#define USER_ID_LEN         24

#define DATA_BUFFER_SIZE    102400

#define CMD_TAG_OFFSET		0
#define FRAMELEN_H_OFFSET	1
#define FRAMELEN_L_OFFSET	2
#define PACK_H_OFFSET		3
#define PACK_L_OFFSET		4
#define MAXPACK_H_OFFSET	5
#define MAXPACK_L_OFFSET	6
#define CMD_TYPE_OFFSET		7
#define CMD_OFFSET		    8
#define INFOLEN_H_OFFSET	9
#define INFOLEN_L_OFFSET	10
#define INFO_DATA_OFFSET	11
#define PARA_DATA_LEN       6
#define APP_CRC_LEN         4


typedef void (*P_FUNC_SENDINFO)(char* sInfo,int nInfolen);
typedef void (*P_FUNC_RECVINFO)(char* cInfo,int nInfolen);
typedef void (*P_FUNC_RESULT)(char* sResult,int nInfolen, int ops_ret);
typedef void (*P_FUNC_RECVDATA)(char cCmd,unsigned char* cData,int nDatalen);

//存放包体的结构
typedef struct BodyData
{
    unsigned char cpkgdata[MAX_DATA_LEN];//包体数据
    int   npkglen;//包体长度
}ST_BodyData;


//************************************
//功能：
//函数名: CharToString
//参数:   T cCmdData
//参数:   int nDataLen
//参数:   bool bHex
//参数:   CString & strOut
//返回值:  void
//说明:
//************************************
template<typename T> void CharToString(T cCmdData,int nDataLen,string &strOut)
{
    strOut.clear();

    stringstream stream;

    for(int i = 0; i < nDataLen; i++)
    {
        stream<<hex<<setw(2)<<setfill('0')<<(int)(cCmdData[i] & 0xFF)<<" ";

    }

    strOut  = stream.str();
    stream.clear();
};

template<typename T> int GetUserIdOffset(T *cRecData, int nDataLen)
{
    //找到ID号中第一位不为0位置
    int i = 0;
    for(i = 0; i < nDataLen; i++)
    {
        if(cRecData[i] != 0)
            break;
    }
    //并将ID不为0的位置偏移量传回
    return (i);
};


//************************************
//功能：  多包下发 T目前为char* 或unsigned char*
//函数名: sendMulti
//参数:   T data
//参数:   int nDatalen
//返回值:  void
//说明:
//************************************
template <typename T> void GetMultiPack(T data,int nDatalen,vector<BodyData>& vecData)
{
    int nMaxpack = nDatalen/MAX_DATA_LEN;
    int nSurplus = nDatalen%MAX_DATA_LEN;
    if (nSurplus >0)
    {
        nMaxpack +=1;
    }

    int nSendLen = 0;
    ST_BodyData  body;
    for (int i = 0;i<nMaxpack;i++)
    {
        if (i == (nMaxpack-1))
        {

            memset(body.cpkgdata,0,MAX_DATA_LEN);
            memcpy(body.cpkgdata,data+nSendLen,nSurplus);
            body.npkglen = nSurplus;
            vecData.push_back(body);
            nSendLen += body.npkglen;
        }
        else
        {
            memset(body.cpkgdata,0,MAX_DATA_LEN);
            memcpy(body.cpkgdata,data+nSendLen,MAX_DATA_LEN);
            body.npkglen = MAX_DATA_LEN;
            vecData.push_back(body);
            nSendLen += body.npkglen;
        }

    }
};

//************************************
//功能：
//函数名: GetMaxPacklen
//参数:   T * cCmData
//返回值:  int
//说明:
//************************************
template<typename T> int GetMaxPacklen(T* cCmData)
{
    int nMaxPack = (cCmData[MAXPACK_H_OFFSET] << 8 & 0xFF00) | (cCmData[MAXPACK_L_OFFSET] & 0xFF);

    return nMaxPack;
};

//************************************
//功能：
//函数名: GetPackIndex
//参数:   T * cCmData
//返回值:  int
//说明:
//************************************
template<typename T> int GetPackIndex(T* cCmData)
{
    int iIndex = (cCmData[PACK_H_OFFSET] << 8 & 0xFF00) | (cCmData[PACK_L_OFFSET] & 0xFF);
    return iIndex;
}

/***********************************************************
函数名：GetFramelen
输入参数：
cCmdData：命令号
bContainHead：是否包含帧头的TAGLEN个字节
函数返回值：
nFrameLen：命令帧的长度
************************************************************/
template<class T> int GetFramelen(T* cCmdData,bool bContainHead = false)
{
    if (NULL == cCmdData)
    {
        return 0;
    }

    int nFramelen = 0;
    nFramelen = (cCmdData[FRAMELEN_H_OFFSET] << 8 & 0xFF00) | (cCmdData[FRAMELEN_L_OFFSET] & 0xFF);
    if(bContainHead)
        nFramelen += TAGLEN;

    return nFramelen;
};

//************************************
//功能：
//函数名: GetDatalen
//参数:   T * cCmdData
//返回值:  int
//说明:
//************************************
template<typename T> int GetDatalen(T* cCmdData)
{
    if (NULL == cCmdData)
    {
        return 0;
    }

    int nDatalen = 0;
    nDatalen = (cCmdData[INFOLEN_H_OFFSET] << 8 & 0xFF00) | (cCmdData[INFOLEN_L_OFFSET] & 0xFF);

    return nDatalen;
};

//************************************
 //功能：  BBC校验
 //函数名: CheckBBC
 //参数:   T * data
 //参数:   int nDataLen
 //返回值:  T
 //说明:
 //************************************
 template<typename T> T  CheckBBC(T* data,int nDataLen)
{
    T cBbc = 0;
    int i = 0;

    for(i = 0; i < (nDataLen - 2); i++)
    {
        cBbc ^= data[i];
    }

    return ((~cBbc)&0xFF);
};


 /***********************************************************
 函数名：IrIsMemcpy
 函数说明： 本函数仿照C的memcpy用来实现内存数据拷贝，
 输入参数：
 cDest：目标字符串
 cSrc：源字符串
 nDataLen：拷贝的长度
 输出参数：
 cDest：目标字符串
 ************************************************************/
 template<typename T,typename T1> void Sky_Memcpy(T* cDest,T1* cSrc,int nDataLen)
 {
     if (cSrc == NULL)
     {
         return;
     }

     while (nDataLen--)
     {
         *cDest++ = *cSrc++;
     }

 };


 //************************************
//功能：  提取回包中的数据
//函数名: Face_GetCmdData
//参数:   char * cmdData
//参数:   char *  & cResult 需要在外部释放
//返回值:  int
//说明:   cResult 需要在外部释放!!
//************************************
template<typename T,typename T1> int Sky_GetCmdData(T* cmdData,T1* &cResult,int &nReslen)
{
    if (NULL == cmdData)
    {
        return -1;
    }

    nReslen = GetDatalen(cmdData);

    cResult = new char[nReslen + 1];
    memset(cResult,0,nReslen + 1);
    Sky_Memcpy(cResult,cmdData+INFO_DATA_OFFSET,nReslen);
    return 0;
};


//************************************
//功能：  按照协议进行组包
//函数名: Sky_GetSendCmdData
//参数:   char cCmd
//参数:   char * cInfoData
//参数:   int nInfoDataLen
//参数:   char * cCmdData
//参数:   int nMaxPack
//参数:   int nPackIndex
//返回值:  int
//说明:
//************************************
template<typename T,typename T1> int Sky_GetSendCmdData(char cCmd,T *cInfoData, int nInfoDataLen,T1 *cCmdData,int nPackIndex=0,int nMaxPack=0)
{

    int nPacklen = 0;
    int nFrameLen = 0;
    char cBbc = 0;

    if(cCmd < START_DEVICE || cCmd > ENROLL_FEEDBACK)
    {
        return -1;
    }

    if(nMaxPack < 0 )
    {
        return -2;
    }

    if (cCmdData == NULL)
    {
        return -3;
    }

    nFrameLen = FRAME_NODATA_LEN + nInfoDataLen;//协议中帧长不包括包头和本身
    nPacklen = NODATA_LEN + nInfoDataLen;

    cBbc ^=cCmdData[CMD_TAG_OFFSET] = CMD_TAGHEX;//包头

    cBbc ^=cCmdData[FRAMELEN_H_OFFSET] = ((nFrameLen >> 8) & 0xFF);//帧长高字节
    cBbc ^=cCmdData[FRAMELEN_L_OFFSET] = (nFrameLen & 0xFF);

    cBbc ^=cCmdData[PACK_H_OFFSET] = ((nPackIndex >> 8) & 0xFF);//当前包长高字节
    cBbc ^=cCmdData[PACK_L_OFFSET] = (nPackIndex & 0xFF);//当前包长低字节

    cBbc ^=cCmdData[MAXPACK_H_OFFSET] = ((nMaxPack >> 8) & 0xFF);//最大包长高字节
    cBbc ^=cCmdData[MAXPACK_L_OFFSET] = (nMaxPack & 0xFF);//最大包长低字节

    cBbc ^=cCmdData[CMD_TYPE_OFFSET] = CMD_TYPE;//命令类型
    cBbc ^=cCmdData[CMD_OFFSET] = cCmd;//命令

    cBbc ^=cCmdData[INFOLEN_H_OFFSET] = ((nInfoDataLen >> 8) & 0xFF);//数据长度高字节
    cBbc ^=cCmdData[INFOLEN_L_OFFSET] = (nInfoDataLen & 0xFF);//数据长度高字节

    //注意，下面的数据拷贝可以根据操作系统的不同使用相应的函数，如memcpy等，需包含相应的头文件
    memcpy(cCmdData+INFO_DATA_OFFSET, cInfoData, nInfoDataLen);

    for (int i = 0; i < nInfoDataLen; i++)
    {
        cBbc ^=cCmdData[INFO_DATA_OFFSET + i] = cInfoData[i];
    }

    cCmdData[INFO_DATA_OFFSET+nInfoDataLen] = ((~cBbc) & 0xFF);

    cCmdData[nPacklen-1] = CMD_ETX;


    return nPacklen;
};

template <typename T> bool CheckRecvData(T* pData,int &iDatalen)
{
    bool bResult = false;
    //如果收到一个帧头和一个帧尾
    if((pData[0] == CMD_TAGHEX))
    {
        int nFramelen = GetFramelen(pData);
        int iPacklen = nFramelen + TAGLEN;
        if(iDatalen == iPacklen )//如果接收到的数据长度和帧长相等，认为可能收到了一帧数据，进行后续处理
        {
            if(pData[iDatalen-1] == CMD_ETX)//找到结束符，为合法帧，否则丢弃，继续接收
            {
                bResult = true;
            }
            else
            {
                memset(pData,0,DATA_BUFFER_SIZE);
                iDatalen = 0;
                bResult = false;
            }
        }
        else if (iDatalen > iPacklen)//超出帧长丢弃后，继续接收
        {
            memset(pData,0,DATA_BUFFER_SIZE);
            iDatalen = 0;
            bResult = false;
        }
        else if (iDatalen < iPacklen) //不够继续接收
        {
            bResult = false;
        }

    }
    else//第一个不是帧头，丢弃，继续接收
    {
        memset(pData,0,DATA_BUFFER_SIZE);
        iDatalen = 0;
        bResult = false;
    }



    return bResult;
};

template <typename T> void ChangeIdToChar(char* userId, T* chuserId)
{
    if(NULL == userId)
    {
        return;
    }

    int nUserIdLen = strlen(userId);
    if(nUserIdLen <= 0)
    {
        return;
    }

    memcpy((chuserId + USER_ID_LEN - nUserIdLen), userId, nUserIdLen);
};

#endif

