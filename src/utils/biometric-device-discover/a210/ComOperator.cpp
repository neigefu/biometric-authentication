#include "ComOperator.h"
#include <iostream>
#include <biometric_intl.h>
#include <biometric_common.h>

using namespace std;

#define MAX_RESULT_LEN	256
static char sResult[MAX_RESULT_LEN] = {0};

CComOperator::CComOperator(void)
{
    m_iEnablelog = 0;
    m_serial.SetParent(this);
    m_pFuncSend = NULL;
    m_pFuncRecv = NULL;
    m_pFuncResult = NULL;
    m_pFuncData = NULL;

    m_bPortOpend = false;

    memset(m_cUserid, 0, USER_ID_LEN + 1);
    memset(m_cRecvBuffer, 0, DATA_BUFFER_SIZE);
    m_pRecvMulData = NULL;

    m_nCurRecvDataCount = 0;
    m_nSendDataCount = 0;
    m_nRecvMullen = 0;
    m_nBodyLen = 0;
    m_nMaxPack = 0;
    m_nPackIndex = 0;

    m_curCmd = 0;
    m_curMode = 1;//当前识别模式
}


CComOperator::~CComOperator(void)
{
    if (NULL != m_pRecvMulData)
    {
        delete[] m_pRecvMulData;
        m_pRecvMulData = NULL;
    }

    m_vecBody.clear();
}

int CComOperator::InitPort(char *Dev, int speed, int databits, int stopbits, char parity)
{
    int ret = m_serial.OpenDev(Dev);
    if (ret == 0)
    {
        m_serial.set_Parity(databits, stopbits, parity, speed);
    }
    else
    {
        m_sResult = _("Can not open serial");
        wrapResultInfo((char*)m_sResult.c_str(), COM_OPS_RET_ERROR);
        return -COM_OPS_RET_ERROR;
    }

    if (ret == 0)
        m_bPortOpend = true;

    return ret;
}

void CComOperator::SetTimeout(int timeout)
{
    m_serial.Timeout = timeout;
}

void CComOperator::SetStopFlag(bool flag)
{
#ifdef COM_OPERATOR_DEBUG
    bio_print_debug("SetStopFlag m_serial = %p\n", &m_serial);
#endif

    m_serial.StopFlag = flag;
}

bool CComOperator::GetStopFlag()
{
    return m_serial.StopFlag;
}

int CComOperator::closePort()
{
    m_serial.CloseDev();
    return 0;
}

int CComOperator::InitCallBack(P_FUNC_SENDINFO funSend, P_FUNC_RECVINFO funRecv, P_FUNC_RESULT funRes, P_FUNC_RECVDATA funData)
{
    if (NULL == funSend || NULL == funRecv || NULL == funRes)
    {
        return 1;
    }

    m_pFuncSend = funSend;
    m_pFuncRecv = funRecv;
    m_pFuncResult = funRes;
    m_pFuncData = funData;

    return 0;
}

int CComOperator::SendData(char cCmd, unsigned char *cData, int nDatalen)
{
    int ret = 0;

    if (!m_bPortOpend)
    {
        m_sResult = "";
        return -1;
    }

    if (0 != VerifyCmdAndData(cCmd, cData, nDatalen))
    {
        wrapResultInfo((char *)m_sResult.c_str(), COM_OPS_RET_ERROR);
        return -1;
    }

    if (nDatalen > MAX_DATA_LEN)
    {
        m_vecBody.clear();
        GetMultiPack(cData, nDatalen, m_vecBody);

        m_nBodyLen = m_vecBody.size();
        m_nMaxPack = m_nBodyLen - 1;

        m_nPackIndex = 0;
        if (m_nBodyLen <= 0)
        {
            return -2;
        }

        SendOnePackData(cCmd, m_vecBody[0].cpkgdata, m_vecBody[0].npkglen);

        snprintf(sResult, MAX_RESULT_LEN, "The %d of %d packet send ok!\n",
                 m_nPackIndex, m_nMaxPack);
        m_sResult = sResult;

        m_nPackIndex++;

        Writelog((char *)m_sResult.c_str());
    }
    else
    {
        m_nPackIndex = 0;
        m_nMaxPack = 0;
        SendOnePackData(cCmd, cData, nDatalen);
    }

    return ret;
}

int CComOperator::SendDataSync(char cCmd,unsigned char* cData,int nDatalen)
{
    int ret = COM_OPS_RET_SUCCESS;
    ret = SendData(cCmd, cData, nDatalen);
    if (ret < 0)
    {
        return ret;
    }

#ifdef COM_OPERATOR_DEBUG
    bio_print_debug("In SendDataSync, m_serial = %p\n", &m_serial);
#endif

    ret = m_serial.WaitingForRead(&m_serial);

    return -ret;
}

void CComOperator::Startlog(int iEnable)
{
    m_iEnablelog = iEnable;
}

void CComOperator::Clearlog()
{
    ofstream ofs("log.txt", ios::trunc);
    if (ofs)
    {
        ofs.close();
    }
}

//************************************
//功能：  解析接收到的数据
//函数名: RecvedCmdDataExtract
//参数:   char * cRecData
//参数:   int nDataLen
//返回值:  void
//说明:
//************************************
int CComOperator::RecvedCmdDataExtract(unsigned char *cRecData, int nDataLen)
{
#ifdef COM_OPERATOR_DEBUG
    static int ops_no = 0;
    ops_no++;
    bio_print_debug(_("No.%d processing starts ...\n"), ops_no);
#endif

    int ret = COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING;

    //如果数据校验和错误，回复Bbc校验错误命令
    if (cRecData[nDataLen - 2] != CheckBBC(cRecData, nDataLen))
    {
        snprintf(sResult, MAX_RESULT_LEN,
                 _("Command: 0x%02X returns data checksum error\n"),
                 cRecData[CMD_OFFSET]);
        m_sResult = sResult;

        ret = COM_OPS_RET_CHECKSUM_ERROR;
        return ret;
    }

    int nResult = 0;
    char chCmd = cRecData[CMD_OFFSET];
    char *cRecvBody = NULL;
    int nBodylen = 0;
    int iOffSet = 0;
    string m_strCmdOpReslutl, m_strCmdOpReslutr;

    wrapRecvInfo(cRecData, nDataLen);
    Sky_GetCmdData(cRecData, cRecvBody, nBodylen);
    string strTemp;

#ifdef COM_OPERATOR_DEBUG
    bio_print_debug("chCMD = %c\n" , chCmd);
#endif

    int len = 0;
    memset(sResult, 0, MAX_RESULT_LEN);
    switch (chCmd)
    {
    case START_DEVICE: //启动设备
        snprintf(sResult, MAX_RESULT_LEN,
                 _("Device started successfully! The current version is: v"));
        len = strlen(sResult);
        for (int i = 0; i < nBodylen; i++)
        {
            len = strlen(sResult);
            if (len + 2 < MAX_RESULT_LEN)
                snprintf(&sResult[len], MAX_RESULT_LEN, "%X",
                cRecvBody[i] & 0xFF);

            len = strlen(sResult);
            if ((len + 1 < MAX_RESULT_LEN) && (i < nBodylen - 1))
                snprintf(&sResult[len], MAX_RESULT_LEN, ".");
        }
        sResult[MAX_RESULT_LEN - 1] = '\0';

        m_sResult = sResult;
        ret = COM_OPS_RET_SUCCESS;
        break;
    case USER_IDENTIFY: //开始识别
        iOffSet = GetUserIdOffset(cRecvBody, nBodylen);
        if (iOffSet == USER_ID_LEN)
        {
            m_sResult = _("Identify failed");
            ret = COM_OPS_RET_FAIL_OR_NO_MATCH;
        }
        else
        {
            strTemp = string((&(cRecvBody[iOffSet])));
            snprintf(sResult, MAX_RESULT_LEN, _("Identify success, ID: %s"),
                     strTemp.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_SUCCESS;
        }
        break;
    case USER_ENROLL:
        if (cRecData[INFO_DATA_OFFSET] == 0x00)
        {
            GetUserId(cRecData + INFO_DATA_OFFSET + 1, USER_ID_LEN, m_strUserId);
            snprintf(sResult, MAX_RESULT_LEN, _("Enroll success, ID: %s"),
                     m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_SUCCESS;
        }
        else if (cRecData[INFO_DATA_OFFSET] == 0x01)
        {
            GetUserId(cRecData + INFO_DATA_OFFSET + 1, USER_ID_LEN, m_strUserId);
            snprintf(sResult, MAX_RESULT_LEN, _("Enroll failed, ID: %s"),
                     m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_ERROR;
        }
        else if (cRecData[INFO_DATA_OFFSET] == 0x02)
        {
            GetUserId(cRecData + INFO_DATA_OFFSET + 1, USER_ID_LEN, m_strUserId);
            snprintf(sResult, MAX_RESULT_LEN,
                     _("Enroll success, the user already exists, correct ID: %s"),
                     m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_SUCCESS;
        }
        else if (cRecData[INFO_DATA_OFFSET] == 0x03)
        {
            GetUserId(cRecData + INFO_DATA_OFFSET + 1, USER_ID_LEN, m_strUserId);
            snprintf(sResult, MAX_RESULT_LEN, _("Enroll success, ID: %s"), m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_ERROR;
        }
        else
        {
            snprintf(sResult, MAX_RESULT_LEN, _("ID: %s enroll failed, ret: 0x%02X"),
                     m_strUserId.c_str(), cRecData[INFO_DATA_OFFSET]);
            m_sResult = sResult;

            ret = COM_OPS_RET_ERROR;
        }
        break;
    case USER_DELETE: //删除模板
        if (cRecData[INFO_DATA_OFFSET] == 0x00)
        {
            snprintf(sResult, MAX_RESULT_LEN, _("Delete user %s template success")
                     , m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_SUCCESS;
        }
        else if (cRecData[INFO_DATA_OFFSET] == 0x01)
        {
            snprintf(sResult, MAX_RESULT_LEN, _("Delete user %s template failed"),
                     m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case READ_USER:
        nResult = RecvMulData(cRecData, nDataLen);
        if (1 == nResult)
        {
            snprintf(sResult, MAX_RESULT_LEN, _("Read user success, user list: %s"),
                     m_strUserList.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_SUCCESS;
        }
        else if (-1 == nResult)
        {
            m_sResult = _("Read user failed");
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case CANCLE_OP: //取消当前命令
        m_sResult = _("Cancel the operation success");
        ret = COM_OPS_RET_SUCCESS;
        break;
    case GET_STATUS: //获取设备状态
        if (cRecData[INFO_DATA_OFFSET] == 0)
        {
            m_sResult = _("Get device status success, device operates normally");
            ret = COM_OPS_RET_SUCCESS;
        }
        else
        {
            m_sResult = _("Device operates abnormally, hardware version does not match");
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case GET_PATTERN:
        nResult = RecvMulData(cRecData, nDataLen);
        if (nResult == 1)
        {
            snprintf(sResult, MAX_RESULT_LEN, _("Get user %s's template success"),
                     m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_SUCCESS;
        }
        else if (-1 == RecvMulData(cRecData, nDataLen))
        {

#ifdef COM_OPERATOR_DEBUG
            bio_print_debug(_("Get user %s's template failed\n"),
                            m_strUserId.c_str());
#endif

            snprintf(sResult, MAX_RESULT_LEN, _("Get user %s's template failed"),
                     m_strUserId.c_str());
            m_sResult = sResult;
            ret = COM_OPS_RET_ERROR;
        }

#ifdef COM_OPERATOR_DEBUG
        bio_print_debug(_("Receive the data of multiple packets, result = %d, ret = %d\n"),
                        nResult, ret);
#endif

        break;
    case SEND_PATTERN:
        if (cRecData[INFO_DATA_OFFSET] == 0)
        {
            m_sResult = _("Issue template success");
            ret = COM_OPS_RET_SUCCESS;
        }
        else
        {
            m_sResult = _("Issue template failed");
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case APP_UPDATE:
        if (cRecData[INFO_DATA_OFFSET] == 0x00)
        {
            m_sResult = _("Update success");
            ret = COM_OPS_RET_SUCCESS;
        }
        else if (cRecData[INFO_DATA_OFFSET] == 0x01)
        {
            m_sResult = _("Update failed");
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case CARD_MODE: //切换识别模式
        if (cRecData[INFO_DATA_OFFSET] == 0)
        {
            if (m_curMode == 1)
            {
                m_sResult = _("Switch mode success, current mode is card-read "
                              "mode, please insert the IC card into the slot");
                ret = COM_OPS_RET_SUCCESS;
            }
            else
            {
                m_sResult = _("Switch mode success, the current mode is local "
                              "recognition mode, identify by the iris library "
                              "that is stored in device");
                ret = COM_OPS_RET_SUCCESS;
            }
        }
        else
        {
            m_sResult = _("Switch mode failed");
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case GET_CARD_ID: //获取卡号
        if (cRecData[INFO_DATA_OFFSET] == 0 && cRecData[INFO_DATA_OFFSET + 1] == 0 && cRecData[INFO_DATA_OFFSET + 2] == 0 && cRecData[INFO_DATA_OFFSET + 3] == 0)
        {
            m_sResult = _("Read the M1 card number failed, please check if the "
                          "card is in the slot");
            ret = COM_OPS_RET_ERROR;
        }
        else
        {
            snprintf(sResult, MAX_RESULT_LEN, _("Read M1 card number success, "
                                                "card number: %X%X%X%X"),
                     cRecData[INFO_DATA_OFFSET], cRecData[INFO_DATA_OFFSET + 1],
                     cRecData[INFO_DATA_OFFSET + 2], cRecData[INFO_DATA_OFFSET + 3]);
            m_sResult = sResult;
            ret = COM_OPS_RET_SUCCESS;
        }
        break;
    case CAPTURE_IMG:
        nResult = RecvMulData(cRecData, nDataLen);
        if (1 == nResult)
        {
            m_sResult = _("Capture image success");
            ret = COM_OPS_RET_SUCCESS;
        }
        else if (-1 == nResult)
        {
            m_sResult = _("Capture image failed");
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case PACK_ACK: //握手包

#ifdef COM_OPERATOR_DEBUG
        bio_print_debug("PACK_ACK\n");
#endif

        SendMulPackData();
        ret = COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING;
        break;
    case BBC_ERROR:
        {
            m_sResult = _("Device BBC check error");
            ret = COM_OPS_RET_ERROR;
        }
        break;
    case ENROLL_PARAM:
        m_sResult = _("Issue configuration parameter success");
        ret = COM_OPS_RET_SUCCESS;
        break;
    case ENROLL_FEEDBACK:
        //左眼
        if (cRecData[INFO_DATA_OFFSET] == 0x00)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s%s %s",_("Keep distance"),
                     _("(Left)"), "O");
            m_strCmdOpReslutl = sResult;	//保持当前状态
        }
        else if(cRecData[INFO_DATA_OFFSET] == 0x01)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s%s %s",_("Adjust distance"),
                     _("(Left)"), "X");
            m_strCmdOpReslutl = sResult;	//距离不对-提示
        }
        else if(cRecData[INFO_DATA_OFFSET] == 0x02)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s%s %s",_("Look in the mirror"),
                     _("(Left)"), "X");
            m_strCmdOpReslutl = sResult;	//眼睛出界-提示
        }
        else if(cRecData[INFO_DATA_OFFSET] == 0x03)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s%s %s",_("Come closer"),
                     _("(Left)"), "X");
            m_strCmdOpReslutl = sResult;	//距离太远-提示
        }
        else if(cRecData[INFO_DATA_OFFSET] == 0x04)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s%s %s",_("Open your eyes"),
                     _("(Left)"), "X");
            m_strCmdOpReslutl = sResult;	//眼睛张合度太小-提示
        }
        else if(cRecData[INFO_DATA_OFFSET] == 0x05)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s%s %s",_("Don't squint"),
                     _("(Left)"), "X");
            m_strCmdOpReslutl = sResult;	//眼睛斜视-提示
        }
        //右眼
        if (cRecData[INFO_DATA_OFFSET + 1] == 0x00)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s %s%s", "O", _("Keep distance"),
                     _("(Right)"));
            m_strCmdOpReslutr = sResult;	//保持当前状态
        }
        else if (cRecData[INFO_DATA_OFFSET + 1] == 0x01)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s %s%s", "X", _("Adjust distance"),
                     _("(Right)"));
            m_strCmdOpReslutr = sResult;	//距离不对-提示
        }
        else if (cRecData[INFO_DATA_OFFSET + 1] == 0x02)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s %s%s", "X", _("Look in the mirror"),
                     _("(Right)"));
            m_strCmdOpReslutr = sResult;	//眼睛出界-提示
        }
        else if (cRecData[INFO_DATA_OFFSET + 1] == 0x03)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s %s%s", "X", _("Come closer"),
                     _("(Right)"));
            m_strCmdOpReslutr = sResult;	//距离太远-提示
        }
        else if (cRecData[INFO_DATA_OFFSET + 1] == 0x04)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s %s%s", "X", _("Open your eyes"),
                     _("(Right)"));
            m_strCmdOpReslutr = sResult;	//眼睛张合度太小-提示
        }
        else if (cRecData[INFO_DATA_OFFSET + 1] == 0x05)
        {
            snprintf(sResult, MAX_RESULT_LEN, "%s %s%s", "X", _("Don't squint"),
                     _("(Right)"));
            m_strCmdOpReslutr = sResult;	//眼睛斜视-提示
        }

        m_sResult = m_strCmdOpReslutl + "_" + m_strCmdOpReslutr;
        ret = COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING;
        break;
    case PRO_REALTIME_TEMP:
        if(cRecData[INFO_DATA_OFFSET] == 0x01)
        {
            m_sResult = _("Generate real-time template success");

            ret = COM_OPS_RET_SUCCESS;
        }
        else if (cRecData[INFO_DATA_OFFSET] == 0x00)
        {
            m_sResult = _("Generate real-time template failed");

            ret = COM_OPS_RET_ERROR;
        }
        break;
    case GET_REALTIME_TEMP:
        nResult = RecvMulData(cRecData, nDataLen);
        if (1 == nResult)
        {
            m_sResult = _("Capture real-time template success");

            ret = COM_OPS_RET_SUCCESS;
        }
        else if (-1 == nResult)
        {
            m_sResult = _("Capture real-time template failed");

            ret = COM_OPS_RET_ERROR;
        }
        break;
    case END_REALTIME_TEMP:
        if (cRecData[INFO_DATA_OFFSET] == 0x01)
        {
            m_sResult = _("Stop generating real-time template success");

            ret = COM_OPS_RET_SUCCESS;
        }
        else if (cRecData[INFO_DATA_OFFSET] == 0x00)
        {
            m_sResult = _("Stop generating real-time template failed");

            ret = COM_OPS_RET_ERROR;
        }
        break;
    default:
        m_sResult = _("Unsupported commands");
        ret = COM_OPS_RET_ERROR;
        break;
    }

    if (NULL != cRecvBody)
    {
        delete[] cRecvBody;
        cRecvBody = NULL;
    }

    wrapResultInfo((char *)m_sResult.c_str(), ret);

    m_nCurRecvDataCount = 0;

#ifdef COM_OPERATOR_DEBUG
    bio_print_debug(_("No.%d processing end, ret = %d ...\n"), ops_no, ret);
#endif

    return ret;
}

//************************************
//功能：  发送单包命令
//函数名: SendOnePackData
//参数:   char cCmd
//参数:   unsigned char * cData
//参数:   int nDatalen
//返回值:  void
//说明:
//************************************
void CComOperator::SendOnePackData(char cCmd,unsigned char * cData, int nDatalen)
{
    unsigned char * cCmdData = NULL;
    cCmdData = new unsigned char[NODATA_LEN + nDatalen];
    memset(cCmdData, 0, (NODATA_LEN + nDatalen));

    int nCmdDataLen = Sky_GetSendCmdData(cCmd, cData, nDatalen, cCmdData, m_nPackIndex, m_nMaxPack);
    if (nCmdDataLen >= NODATA_LEN && nCmdDataLen <= MAX_PACK_LEN)
    {
        m_serial.WriteToPort(cCmdData, nCmdDataLen);
        wrapSendInfo(cCmdData, nCmdDataLen);

        string sdata;
        CharToString(cCmdData, nCmdDataLen, sdata);
        string slog = _("Current send data: ");
        slog += sdata;
        Writelog(slog);
    }

    if (NULL != cCmdData)
    {
        delete[] cCmdData;
        cCmdData = NULL;
    }
}

//************************************
//功能：  发送多包命令
//函数名: SendMulPackData
//返回值:  void
//说明:
//************************************
void CComOperator::SendMulPackData()
{
    if (m_nPackIndex < m_nBodyLen)
    {
        unsigned char * Data = NULL;
        int nDatalen = 0;
        Data = m_vecBody[m_nPackIndex].cpkgdata;
        nDatalen = m_vecBody[m_nPackIndex].npkglen;

        SendOnePackData(m_curCmd, Data, nDatalen);

        snprintf(sResult, MAX_RESULT_LEN,
                 "SendMulPackData: The %d of %d packet send ok!\n",
                 m_nPackIndex, m_nMaxPack);
        m_sResult = sResult;

        m_nSendDataCount += nDatalen;
        m_nPackIndex++;

        wrapResultInfo((char *)m_sResult.c_str(), COM_OPS_RET_SUCCESS);

        string slog = _("Current send data: ");
        slog += m_sResult;
        Writelog((char *)slog.c_str());
    }
}

//************************************
//功能：  接收多包数据
//函数名: RecvMulData
//参数:   unsigned char * cRecData
//参数:   int nDataLen
//返回值:  int 0-执行成功，1-读取完毕,-1-操作失败
//说明:
//************************************
int CComOperator::RecvMulData(unsigned char *cRecData, int nDataLen)
{
    int ret = 0;
    int nMaxPack = GetMaxPacklen(cRecData);
    int iIndex = GetPackIndex(cRecData);
    int iCurDatalen = GetDatalen(cRecData);

    if (nMaxPack > 0)	//多包
    {

        if (iIndex == 0)
        {
            if (m_pRecvMulData)
            {
                free(m_pRecvMulData);
                m_pRecvMulData = NULL;
            }

            m_pRecvMulData = new unsigned char[MAX_DATA_LEN * (nMaxPack + 1) + 1];
            memset(m_pRecvMulData, 0, MAX_DATA_LEN * (nMaxPack + 1) + 1);

            snprintf(sResult, MAX_RESULT_LEN, "The %d of %d packet Recv OK!\n",
                     iIndex, nMaxPack);
            m_sResult = sResult;

            m_nRecvMullen = 0;
            GetDataHandler(cRecData, iCurDatalen);
            SendAck();
        }
        else if (iIndex == nMaxPack)
        {
            GetDataHandler(cRecData, iCurDatalen);

            snprintf(sResult, MAX_RESULT_LEN, "The %d of %d packet Recv OK!\n",
                     iIndex, nMaxPack);
            m_sResult = sResult;

            ret = 1;
            if (0 == iCurDatalen)
            {
                ret = -1;
            }

            if (m_curCmd == READ_USER)
            {
                GetUserList(m_pRecvMulData, m_nRecvMullen);
                if (m_strUserList.empty())
                {
                    ret = -1;
                }
                else
                {
                    ret = 1;
                }
            }

            wrapRecvData(m_curCmd, m_pRecvMulData, m_nRecvMullen);

            if (NULL != m_pRecvMulData)
            {
                delete[] m_pRecvMulData;
                m_pRecvMulData = NULL;
            }
        }
        else
        {
            GetDataHandler(cRecData, iCurDatalen);
            SendAck();

            snprintf(sResult, MAX_RESULT_LEN, "The %d of %d packet Recv OK!\n",
                     iIndex, nMaxPack);
            m_sResult = sResult;
        }
    }
    else//单包
    {
        if (m_pRecvMulData)
        {
            free(m_pRecvMulData);
            m_pRecvMulData = NULL;
        }

        m_pRecvMulData = new unsigned char[MAX_DATA_LEN * (nMaxPack + 1) + 1];
        memset(m_pRecvMulData, 0, MAX_DATA_LEN * (nMaxPack + 1) + 1);

        m_nRecvMullen = 0;
        GetDataHandler(cRecData, iCurDatalen);
        ret = 1;

        if (0 == iCurDatalen)
        {
            ret = -1;
        }

        if (m_curCmd == READ_USER)
        {
            GetUserList(m_pRecvMulData, m_nRecvMullen);
            if (m_strUserList.empty())
            {
                ret = -1;
            }
            else
            {
                ret = 1;
            }
        }

        wrapRecvData(m_curCmd, m_pRecvMulData, m_nRecvMullen);

        if (NULL != m_pRecvMulData)
        {
            delete[] m_pRecvMulData;
            m_pRecvMulData = NULL;
        }
    }

    return ret;
}

//************************************
//功能：  提取包体数据 保存到全局变量 g_pRecvMulData中
//函数名: GetDataHandler
//参数:   unsigned char * cData
//参数:   int nDataLen
//返回值:  void
//说明:
//************************************
void CComOperator::GetDataHandler(unsigned char *cData, int nDataLen)
{
    memcpy(m_pRecvMulData + m_nRecvMullen, (cData + INFO_DATA_OFFSET), nDataLen);
    m_nRecvMullen += nDataLen;
}

//************************************
//功能：  应答包
//函数名: SendAck
//返回值:  void
//说明:
//************************************
void CComOperator::SendAck()
{
    //判断串口是否打开
    if (!m_bPortOpend)
    {
        m_sResult = _("Serial port is not opened, please open it");
        wrapResultInfo((char *)m_sResult.c_str(), COM_OPS_RET_ERROR);
        return;
    }

    int nInfoDataLen = 0;
    unsigned char* cInfoData = NULL;
    SendOnePackData((char)PACK_ACK,cInfoData,nInfoDataLen);
}

void CComOperator::wrapSendInfo(unsigned char *cInfo, int nInfolen)
{
    if (NULL == m_pFuncSend)
        return;

    string strInfo;
    CharToString(cInfo, nInfolen, strInfo);
    char *cData = (char *)strInfo.c_str();
    m_pFuncSend(cData, strInfo.length());
}

void CComOperator::wrapRecvInfo(unsigned char *cInfo, int nInfolen)
{
    if (NULL == m_pFuncRecv)
        return;

    string strInfo;
    CharToString(cInfo, nInfolen, strInfo);
    char *cData = (char *)strInfo.c_str();
    m_pFuncRecv(cData, strInfo.length());
//	m_serial.g_bRunning = false;
}

void CComOperator::wrapRecvData(char cCmd, unsigned char *cData, int nDatalen)
{
    if (NULL == m_pFuncData || NULL == cData)
        return;

    m_pFuncData(cCmd, cData, nDatalen);
}

void CComOperator::wrapResultInfo(char *sInfo, int ops_ret)
{
    if (0 == m_pFuncResult || sInfo == NULL)
        return;

    int ilen = strlen(sInfo);
    m_pFuncResult(sInfo, ilen, ops_ret);
}

int CComOperator::VerifyCmdAndData(char cCmd, unsigned char *cData, int nDatalen)
{
    int ret = 0;
    m_curCmd = cCmd;

    switch (cCmd)
    {
    case START_DEVICE:
        if (nDatalen > 0)
        {
            m_sResult = _("This command does not need to issue data");
            ret = -1;
        }
        break;
    case USER_IDENTIFY:
        if (nDatalen != USER_ID_LEN)
        {
            m_sResult = _("User ID length error");
            ret = -1;
        }
        else
        {
            GetCurUserId(cCmd, cData, nDatalen);
        }
        break;
    case USER_ENROLL:
        if (nDatalen != USER_ID_LEN)
        {
            m_sResult = _("User ID length error");
            ret = -1;
        }
        else
        {
            GetCurUserId(cCmd, cData, nDatalen);
        }
        break;
    case USER_DELETE:
        if (nDatalen != USER_ID_LEN)
        {
            m_sResult = _("User ID length error");
            ret = -1;
        }
        else
        {
            GetCurUserId(cCmd, cData, nDatalen);
        }
        break;
    case READ_USER:
        if (nDatalen != USER_ID_LEN)
        {
            m_sResult = _("User ID length error");
            ret = -1;
        }
        else
        {
            GetCurUserId(cCmd, cData, nDatalen);
        }
        break;
    case CANCLE_OP:
        if (nDatalen > 0)
        {
            m_sResult = _("This command does not need to issue data");
            ret = -1;
        }
        break;
    case GET_STATUS:
        if (nDatalen > 0)
        {
            m_sResult = _("This command does not need to issue data");
            ret = -1;
        }
        break;
    case GET_PATTERN:
        if (nDatalen != USER_ID_LEN)
        {
            m_sResult = _("User ID length error");
            ret = -1;
        }
        else
        {
            GetCurUserId(cCmd, cData, nDatalen);
        }
        break;
    case SEND_PATTERN:
        if (nDatalen != (USER_ID_LEN + 2 * MAX_DATA_LEN))
        {
            m_sResult = _("Template file or user ID length error");
            ret = -1;
        }
        else
        {
            GetCurUserId(cCmd, cData, nDatalen);
        }
        break;
    case APP_UPDATE:
        if (nDatalen <= 0)
        {
            m_sResult = _("File length error");
            ret = -1;
        }
        break;
    case CARD_MODE:
        if (nDatalen != 1)
        {
            m_sResult = _("Mode setting error");
            return -1;
        }
        else
        {
            m_curMode = cData[0] & 0xFF;
        }
        break;
    case GET_CARD_ID:
        if (nDatalen > 0)
        {
            m_sResult = _("This command does not need to issue data");
            ret = -1;
        }
        break;
    case CAPTURE_IMG:
        if (nDatalen > 0)
        {
            m_sResult = _("This command does not need to issue data");
            ret = -1;
        }
        break;
    case PACK_ACK:
        m_sResult = _("Not allowed to issue the interface internal command");
        ret = -1;
        break;
    case BBC_ERROR:
        m_sResult = _("Not allowed to issue the interface internal command");
        ret = -1;
        break;
    case ENROLL_PARAM:
        if (nDatalen != PARA_DATA_LEN)
        {
            m_sResult = _("Parameter length error");
            ret = -1;
        }
        break;
    case ENROLL_FEEDBACK:
        m_sResult = _("Not allowed to issue the interface internal command");
        ret = -1;
        break;
    default:
        m_sResult = _("Unsupported commands");
        ret = -1;
        break;
    }

    return ret;
}

void CComOperator::SetReadlen(int ilen)
{
    m_serial.SetReadlen(ilen);
}

void CComOperator::Writelog(char *slog)
{
    if (m_iEnablelog != 0)
    {
        return;
    }

    ofstream ofs("log.txt", ios::app);
    if (ofs)
    {
        ofs << Getlocaltime() << slog << endl;
        ofs.close();
    }
}

void CComOperator::Writelog(string strlog)
{
    Writelog((char *)strlog.c_str());
}

std::string CComOperator::Getlocaltime()
{
    time_t now_time;
    now_time = time(NULL);
    tm *pCurTime = NULL;
    pCurTime = localtime(&now_time);
    if (NULL == pCurTime)
    {
        return "";
    }

    stringstream stringtime;
    stringtime << 1900 + pCurTime->tm_year << "-" << pCurTime->tm_mon + 1 << "-" << pCurTime->tm_mday
               << " " << pCurTime->tm_hour << ":" << pCurTime->tm_min << ":" << pCurTime->tm_sec << ": ";

    string stime = stringtime.str();

    stringtime.str("");
    stringtime.clear();

    return stime;
}

void CComOperator::GetCurUserId(char cCmd, unsigned char *data, int nDatalen)
{
    if (NULL == data)
    {
        return;
    }
    int iOffSet = GetUserIdOffset(data, nDatalen);
    if (iOffSet < USER_ID_LEN)
    {
        memset(m_cUserid, 0, USER_ID_LEN + 1);
        memcpy(m_cUserid, data + iOffSet, USER_ID_LEN - iOffSet);
        m_strUserId = m_cUserid;
    }
    else
    {
        m_strUserId = _("All user");
    }
}

void CComOperator::GetUserList(unsigned char *cData, int nDatalen)
{
    string sUserName;
    m_strUserList.clear();
    int nUserCount = nDatalen / USER_ID_LEN;
    for (int i = 0; i < nUserCount; i++)
    {
        memset(m_cUserid, 0, USER_ID_LEN);
        memcpy(m_cUserid, cData + (i * USER_ID_LEN), USER_ID_LEN);
        int nIdOffset = GetUserIdOffset(m_cUserid, USER_ID_LEN);
        sUserName = string((&(m_cUserid[nIdOffset])));
        if (sUserName.empty())
        {
            continue;
        }

        m_strUserList += sUserName;
        if (i < nUserCount - 1)
        {
            m_strUserList += ",";
        }
    }
}

void CComOperator::GetUserId(unsigned char *data, int nDatalen, string &sUserId)
{
    sUserId.clear();
    int iOffSet = GetUserIdOffset(data, nDatalen);
    if (iOffSet < USER_ID_LEN)
    {
        memset(m_cUserid, 0, USER_ID_LEN + 1);
        memcpy(m_cUserid, data + iOffSet, USER_ID_LEN - iOffSet);
        sUserId = m_cUserid;
    }
    else
    {
        sUserId = "";
    }
}

int CComOperator::RecveData(CComOperator *pRhs, char *pRecvData, int nRecvLen)
{
    if (NULL == pRecvData)
    {
        return -1;
    }

    if (pRhs == NULL)
    {
        return -1;
    }

    string sInfo;
    string stemp;
    CharToString(pRecvData, nRecvLen, stemp);

    snprintf(sResult, MAX_RESULT_LEN,
             _("Receiving %d bytes of data from a serial port: %s"),
             nRecvLen, stemp.c_str());
    sInfo = sResult;
    pRhs->Writelog(sInfo);

    memset(pRhs->m_cRecvBuffer + pRhs->m_nCurRecvDataCount, 0, nRecvLen);
    memcpy(pRhs->m_cRecvBuffer + pRhs->m_nCurRecvDataCount, pRecvData, nRecvLen);

    pRhs->m_nCurRecvDataCount += nRecvLen;

    int ret = COM_OPS_RET_SUCCESS;
    //判断是不是合法的数据包
    if (CheckRecvData(pRhs->m_cRecvBuffer, pRhs->m_nCurRecvDataCount))
    {
        sInfo = _("Legal data packets");
        pRhs->Writelog(sInfo);
        ret = pRhs->RecvedCmdDataExtract(pRhs->m_cRecvBuffer, pRhs->m_nCurRecvDataCount);

#ifdef COM_OPERATOR_DEBUG
        bio_print_debug("%s\n", sInfo.c_str());
#endif
    }
    else
    {
        sInfo = _("Illegal data packets");
        pRhs->Writelog(sInfo);
//		ret = COM_OPS_RET_ILLEGAL_DATA_PACKAGE;
        ret = COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING;

#ifdef COM_OPERATOR_DEBUG
        bio_print_debug("%s\n", sInfo.c_str());
#endif
    }

    return ret;
}
