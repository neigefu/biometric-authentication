#pragma once
#include "Serial.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdbool.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "errno.h"
#include "fcntl.h"
#include <algorithm>
#include <vector>
#include "math.h"
#include <iostream>
#include<iomanip>
#include<fstream>
#include<string>
#include <sstream>
#include <sys/wait.h>
#include <map>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

using namespace std;

class CSerial
{
public:
    CSerial(void);
    ~CSerial(void);
public:
    void set_speed(int speed);
    int set_Parity(int databits,int stopbits,char parity,int ispeed);
    int OpenDev(char *Dev);
    void CloseDev();
    int StartMonitoring();
    void  SetParent(void* pParent);
    int WriteToPort(unsigned char* chData,int nlen);
private:
    int m_fd;
    pthread_t hSvrThread;
    void*     m_pParent;
protected:
    int speed_arr[8];
    int name_arr[8];
public:
    static void* threadRead(void* ptr);
    int WaitingForRead(void *ptr);
    void SetReadlen(int ilen);
    int  GetReadlen();
public:
    int m_nReadlen;
    bool NeedRunning;
    long Timeout;
    bool StopFlag;
};

enum SerialRet {
    SERIAL_SUCCESS = 0,
    SERIAL_ERROR,
    SERIAL_STOP_BY_USER,
    SERIAL_TIMEOUT,
    SERIAL_MAX,
};
