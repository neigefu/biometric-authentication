/*
 * Copyright (C) 2018 Tianjin KYLIN Information Technology Co., Ltd.
 *
 * Author: Droiing <jianglinxuan@kylinos.cn>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef R301_H
#define R301_H

#include <unistd.h>
#include <termios.h>

#include <biometric_common.h>

typedef unsigned char	uint8;
typedef unsigned short	uint16;
typedef unsigned int	uint32;
#define CheckSumSize	2

#define PKG_BUF_LEN	1024
static uint8 PackageBuffer[PKG_BUF_LEN];
static uint32 addr = 0xffffffff;

#pragma pack(1)
typedef struct PackageHeader_t {
	uint16 	prefix;
	uint32	addr;
	uint8	id;
	uint16	length;
} PackageHeader;
#define PKG_HEAD_LEN sizeof(PackageHeader)

typedef struct CommandPackage_t {
	PackageHeader head;
	uint8	command;
	uint8	arg[PKG_BUF_LEN - PKG_HEAD_LEN - sizeof(uint8)];
} CommandPackage;
#define CMD_PKG_LEN sizeof(CommandPackage)

typedef struct DataPackage_t {
	PackageHeader head;
	uint8	data[PKG_BUF_LEN - PKG_HEAD_LEN];
} DataPackage;
#define DATA_PKG_LEN sizeof(DataPackage)

typedef struct FinishPackage_t {
	PackageHeader head;
	uint8	data[PKG_BUF_LEN - PKG_HEAD_LEN];
} FinishPackage;
#define FIS_PKG_LEN sizeof(FinishPackage)

typedef struct RespondPackage_t {
	PackageHeader head;
	uint8	code;
	uint8	arg[PKG_BUF_LEN - PKG_HEAD_LEN - sizeof(uint8)];
} RespondPackage;
#define RSP_PKG_LEN sizeof(RespondPackage)
#pragma pack()

/*命令*/
#define CMD_GetImage		0x01   	//从传感器上读入图像存于图像缓冲区
#define CMD_GenChar		0x02   	//根据原始图像生成指纹特征存于CharBuffer1或CharBuffer2
#define CMD_Match		0x03   	//精确比对CharBuffer1与CharBuffer2中的特征文件
#define CMD_Search		0x04   	//以CharBuffer1或CharBuffer2中的特征文件搜索整个或部分指
#define CMD_RegModel		0x05   	//将CharBuffer1与CharBuffer2中的特征文件合并生成模板存于CharBuffer2
#define CMD_StoreChar		0x06   	//将特征缓冲区中的文件储存到flash指纹库中
#define CMD_LoadChar		0x07   	//从flash指纹库中读取一个模板到特征缓冲区
#define CMD_UpChar		0x08   	//将特征缓冲区中的文件上传给上位机
#define CMD_DownChar		0x09   	//从上位机下载一个特征文件到特征缓冲区
#define CMD_UpImage      	0x0A   	//上传原始图像
#define CMD_DownImage    	0x0B   	//下载原始图像
#define CMD_DeletChar    	0x0C   	//删除flash指纹库中的一个特征文件
#define CMD_Empty        	0x0D   	//清空flash指纹库
#define CMD_WriteReg     	0x0E   	//写SOC系统寄存器
#define CMD_ReadSysPara  	0x0F   	//读系统基本参数
#define CMD_Enroll		0x10	//注册模板
#define CMD_Identify     	0x11  	//验证指纹
#define CMD_SetPwd       	0x12   	//设置握手设备口令
#define CMD_VfyPwd       	0x13   	//验证握手设备口令
#define CMD_GetRandomCode	0x14	//采样随机数
#define CMD_SetAddr		0x15	//设置模块地址
#define CMD_PortControl		0x17	//通讯端口开关控制
#define CMD_WriteNotepad 	0x18   	//写记事本
#define CMD_ReadNotepad  	0x19   	//读记事本
#define CMD_HighSpeedSearch	0x1B	//高速搜索
#define CMD_GenBinImage		0x1C	//生成二值化指纹图像
#define CMD_alidTempleteNum	0x1D	//读取有效模板个数

/*包头*/
#define FramePrefix		0xEF01	//帧界符

/*包标识*/
#define BagID_CMD 		0x01	//命令包标识
#define BagID_Res       	0x07	//应答包标识
#define BagID_Data      	0x02	//数据包标识
#define BagID_Finish    	0x08	//结束包标识

//确认码
#define R_Success		0x00   	//表示指令执行完毕或OK
#define R_DataError		0x01   	//表示数据包接收错误
#define R_NoFinger		0x02   	//表示传感器上没有手指
#define R_GetImageFail		0x03	//指纹录入失败
#define R_GenCharFail1		0x04	//指纹太干，生成特征失败
#define R_GenCharFail2         	0x05   	//表示指纹图像太湿、太糊而生不成特征
#define R_Disorde       	0x06   	//表示指纹图像太乱而生不成特征
#define R_Nochar        	0x07   	//特征点太少（或面积太小）而生不成特征
#define R_MatchFail        	0x08   	//指纹不匹配
#define R_SearchFail		0x09	//没有搜索到指纹
#define R_Unitelost     	0x0A   	//表示特征合并失败
#define R_OverDataBase     	0x0B   	//访问指纹库时地址序号超出指纹库范围
#define R_ReadCharFail		0x0C	//从指纹库读模板出错或无效
#define R_UpCharFail		0x0D	//上传特征失败
#define R_DataAbort		0x0E	//模块不能接收后续数据包
#define	R_UpImageFail		0x0F	//上传图像失败
#define R_DeleteModeFail	0x10	//删除模板失败
#define R_EmptyFail		0x11	//清空指纹库失败
#define R_CommandError		0x13	//口令不正确
#define R_Noavail       	0x15   	//表示缓冲区内没有有效原始图而生不成特征
#define R_Flashlost     	0x18   	//表示读写FLASH出错
#define R_UdefineError	     	0x19   	//未定义错误
#define	R_InvaidReg		0x1A	//无效寄存器号
#define	R_RegError		0x1B	//寄存器设定内容错误
#define	R_NotePageErr		0x1C	//记事本页码指定错误
#define	R_PortOptFail		0x1D	//端口操作失败

//端口控制
#define Control_OFF		0x00	//端口关闭
#define Control_ON		0x01	//端口打开

static struct termios oldtio;

#endif // R301_H

