/*
 * Copyright (C) 2018 Tianjin KYLIN Information Technology Co., Ltd.
 *
 * Author: Droiing <jianglinxuan@kylinos.cn>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <dlfcn.h>
#include <unistd.h>
#include <math.h>
#include <sys/fcntl.h>
#include <arpa/inet.h>
#include <time.h>

#include <biometric_common.h>
#include <biometric_stroge.h>
#include <biometric_intl.h>
#include <biometric_version.h>

#include "driver_ids.h"
#include "r301.h"

uint8 pack[10];
uint8 bufferdata[1000];
char * capture_base64 = NULL;

void R301OpsTimeusedReset() {
	r301_timeused = 0;
}

void R301OpsTimeusedSleepMS(int ms) {
	usleep(ms * 1000);
	r301_timeused += ms;
}

bool R301OpsIsTimeout() {
	if (r301_timeused > r301_timeout)
		return true;
	else
		return false;
}

void R301OpsElapsedTime(int ms) {
	r301_timeused += ms;
}

int R301SendData(bio_dev * dev, uint8 *data, int count)
{
	int ret = 0;

	if (dev->serial_info.fd < 0){
		bio_print_warning(_("No R301 finger print device detected\n"));
		return -1;
	}

#ifdef TEST_DEBUG
//	printf("count:%d,SendData:",count);
//	int i;
//	for (i=0; i<count; i++)
//		printf("%02x",data[i]);
//	printf("\n");
#endif
	ret = write(dev->serial_info.fd, data, count);

	return ret;
}

int R301RecvData(bio_dev * dev, uint8 *data, int count)
{
	int ret = 0;
	int len = 0;

	if (dev->serial_info.fd < 0){
		bio_print_error(_("No R301 finger print device detected\n"));
		return -1;
	}

	struct timeval start, now;
	gettimeofday(&start, NULL);
	gettimeofday(&now, NULL);
	int elapsed_ms = 0;
	while (elapsed_ms < READ_TIMEOUT_MS && len < count)
	{
		usleep(READ_INTERVAL_MS * 1000);
		ret = read(dev->serial_info.fd, data + len, count - len);
		len = len + ret;
		gettimeofday(&now, NULL);
		elapsed_ms = (now.tv_sec - start.tv_sec) * 1000
					 + (now.tv_usec - start.tv_usec) / 1000;
	}
	printf("count = %d, len = %d\n", count, len);

	return len;
}

static uint16 R301ChecksumGen(bio_dev * dev, uint8 *data,int length)
{
	uint16	checksum = 0;
	int i;

	for (i=0;i<length;i++ )
		checksum += data[i];

	return checksum;
}

static int R301RespondParse(bio_dev * dev, int length)
{
	int ret = 0;
	uint16 checksum = 0;
	RespondPackage *Respond_p = (RespondPackage *)PackageBuffer;

	if (Respond_p->head.id != BagID_Res)
	{
		bio_print_error(_("R301RespondParse: Not a respond package\n"));
		ret = -1;
		goto out;
	}

	if (ntohs(Respond_p->head.length) != (length - sizeof(PackageHeader)))
	{
		bio_print_error(_("R301RespondParse: Respond package length error\n"));
		ret = -1;
		goto out;
	}

	checksum = R301ChecksumGen(dev, &PackageBuffer[6],length - 8);

	/*接收应答*/
	if ((PackageBuffer[length -2] != ((checksum >> 8)&0xff)) ||
				(PackageBuffer[length -1] != (checksum&0xff)))
	{
		bio_print_error(_("R301RespondParse: Respond package checksum error\n"));
		ret = -1;
		goto out;
	}

	ret = Respond_p->code;
out:
	return ret;
}

/************************************************************************
函数名称： R301SendDataPackage
功能说明： 发送数据包
传入参数： data(要发送的数据)
		   length(数据长度)
输出参数： int（成功为0，失败为非0）
************************************************************************/
int R301SendDataPackage(bio_dev * dev, uint8 * data, int length)
{
		int ret;
		DataPackage *data_p = (DataPackage *)PackageBuffer;
		uint16 checksum;

	/*封装数据包*/
	data_p->head.prefix = htons(FramePrefix);
	data_p->head.addr = htonl(addr);
	data_p->head.id = BagID_Data;
	data_p->head.length = htons(CheckSumSize + length);

	if (data != NULL)
	{
		memcpy(data_p->data, data, length);
	}

	/*计算校验和*/
	checksum = R301ChecksumGen(dev, &PackageBuffer[6],length + 3);
	data_p->data[length] = (checksum >> 8)&0xff;
	data_p->data[length + 1] = checksum&0xff;

	/*发送数据包*/
	ret = R301SendData(dev, PackageBuffer,length+11);
	if (ret < length)
	{
		bio_print_error(_("R301 send data package error\n"));
		ret = -1;
		goto out;
	}

	return 0;
out:
	return ret;
}

/************************************************************************
函数名称： SendCommand
功能说明： 发送指令包并接收应答包
传入参数： command(指令代码)
	   arg(输入参数，包含BufferID,StartPage,PageNum等)
	   arg_len(输入参数长度)
	   res(返回参数，包含PageID，MatchScore等)
	   respond_len(返回参数长度)
输出参数： int（成功为0，失败为非0）
************************************************************************/
int R301SendCommand(bio_dev * dev, uint8 command, uint8 *arg, int arg_len,
					uint8 *res, int respond_len)
{
	int ret;
	int length;
	CommandPackage *command_p = (CommandPackage *)PackageBuffer;
	RespondPackage *respond_p = (RespondPackage *)PackageBuffer;
	uint16 checksum;


	/*封装命令*/
	command_p->head.prefix = htons(FramePrefix);
	command_p->head.addr = htonl(addr);
	command_p->head.id = BagID_CMD;
	command_p->head.length = htons(1 + CheckSumSize + arg_len);
	command_p->command = command;

	if (arg !=NULL)
	{
		memcpy(command_p->arg,arg,arg_len);
	}

	length = sizeof(PackageHeader) + 1 + CheckSumSize + arg_len;
	checksum = R301ChecksumGen(dev, &PackageBuffer[6],length - 8);
	command_p->arg[arg_len] = (checksum >> 8)&0xff;
	command_p->arg[arg_len + 1] = checksum&0xff;

	/*发送命令*/
	ret = R301SendData(dev, PackageBuffer, length);
	if (ret < length)
	{
		bio_print_error(_("R301 send command error\n"));
		ret = -1;
		goto out;
	}

	/*接收应答*/
	memset(PackageBuffer,0,48);

	length = sizeof(PackageHeader) + 1 + CheckSumSize + respond_len;
	ret = R301RecvData(dev, PackageBuffer,length);
	if (ret < length)
	{
		bio_print_error(_("R301 receive respond error\n"));
		ret = -1;
		goto out;
	}

	ret = R301RespondParse(dev, length);
	if (ret == 0)
	{
		if (res != NULL)
			memcpy(res, respond_p->arg, respond_len);
	}
	else
		goto out;

	return 0;
out:
	return ret;
}

/************************************************************************
函数名称： R301SendFinish
功能说明： 发送结束包
传入参数： data(结束包的数据内容)
		   length(结束包的数据长度)
输出参数： int（成功为0，失败为非0）
************************************************************************/
int R301SendFinish(bio_dev * dev, uint8 *data, int length)
{
	int ret = 0;
	FinishPackage *Finish_p = (FinishPackage *)PackageBuffer;
	uint16 checksum = 0;

	/*封装命令*/
	Finish_p->head.prefix = htons(FramePrefix);
	Finish_p->head.addr = htonl(addr);
	Finish_p->head.id = BagID_Finish;
	Finish_p->head.length = htons(CheckSumSize + length);

	if (data != NULL){
		memcpy(Finish_p->data,data,length);
	}

	checksum = R301ChecksumGen(dev, &PackageBuffer[6],length + 3);
	Finish_p->data[length] = (checksum >> 8)&0xff;
	Finish_p->data[length + 1] = checksum&0xff;

	/*发送命令*/
	ret = R301SendData(dev, PackageBuffer,length + 11);
	if (ret < length){
		bio_print_error(_("R301 send command error\n"));
		ret = -1;
	}

	return ret;
}

/************************************************************************
函数名称： R301ReadSysPara
功能说明： 读指纹模块基本参数
传入参数： 无
输出参数： int（成功返回基本参数，失败返回-1）
************************************************************************/
int R301ReadSysPara(bio_dev * dev, uint8 res[16])
{
	int ret = 0;

	ret = R301SendCommand(dev, CMD_ReadSysPara, NULL, 0, res, 16);
	if (ret != 0 ){
        R301SetNotify(dev, ret);
        //bio_print_error(_("Get basic parameters failed\n"));
		goto out;
	}

	return ret;

out:
	return -1;

}

static int R301DeviceSet(int fd, int Speed, int Bits, char Event, int Stop)
{
	struct termios newtio;

	if (tcgetattr(fd, &oldtio) != 0){
		bio_print_error(_("Get serial attribute failed\n"));
		return -1;
	}

	bzero(&newtio,sizeof(newtio));
	newtio.c_cflag |= CLOCAL |CREAD;
	newtio.c_cflag &= ~CSIZE;

	switch (Bits) {
	case 7:
		newtio.c_cflag |= CS7;
		break;
	case 8:
		newtio.c_cflag |= CS8;
		break;
	}

	switch (Event) {
	case 'O':
		newtio.c_cflag |= PARENB;
		newtio.c_cflag |= PARODD;
		newtio.c_iflag |= (INPCK | ISTRIP);
		break;
	case 'E':
		newtio.c_iflag |= (INPCK |ISTRIP);
		newtio.c_cflag |= PARENB;
		newtio.c_cflag &= ~PARODD;
		break;
	case 'N':
		newtio.c_cflag &= ~PARENB;
		break;
	}

	switch (Speed) {
	case 2400:
		cfsetispeed(&newtio,B2400);
		cfsetospeed(&newtio,B2400);
		break;
	case 4800:
		cfsetispeed(&newtio,B4800);
		cfsetospeed(&newtio,B4800);
		break;
	case 9600:
		cfsetispeed(&newtio,B9600);
		cfsetospeed(&newtio,B9600);
		break;
	case 57600:
		cfsetispeed(&newtio,B57600);
		cfsetospeed(&newtio,B57600);
		break;
	case 115200:
		cfsetispeed(&newtio,B115200);
		cfsetospeed(&newtio,B115200);
		break;
	case 460800:
		cfsetispeed(&newtio,B460800);
		cfsetospeed(&newtio,B460800);
		break;
	default:
		cfsetispeed(&newtio,B9600);
		cfsetospeed(&newtio,B9600);
		break;
	}

	if (Stop == 1) {
		newtio.c_cflag &= ~CSTOPB;
	} else if (Stop ==2) {
		newtio.c_cflag |= CSTOPB;
	}

	newtio.c_cc[VTIME] = READ_TIMEOUT_MS / 100;
	newtio.c_cc[VMIN] = 0;

	tcflush(fd,TCIFLUSH);
	if ((tcsetattr(fd, TCSANOW, &newtio)) != 0)
	{
		bio_print_error(_("Set serial failed\n"));
		return -1;
	}

	return 0;
}

/************************************************************************
函数名称： R301DownChar
功能说明： 下载指定指纹特征文件到指纹模块缓冲区2
传入参数： data(数据包以及结束包数据)
输出参数： int（成功返回0，失败返回错误码）
************************************************************************/
int R301DownChar(bio_dev * dev, uint8 * data, int datalength)
{
	int ret = 0;
	uint8 para[16];
	uint8 arg[1];
	int pkgsize = 0;
	int i = 0;
	int j = 0;
	int count = 0;

	R301ReadSysPara(dev, para);
	pkgsize = 32 * ((int)pow(2, ((para[12] << 8) + para[13]))) ;
	pkgsize -= 11;
	if (datalength % pkgsize)
		count = datalength / pkgsize + 1;
	else
		count = datalength / pkgsize;

	arg[0] = 2;		// 本地特征文件 -> buffer2
	// 发送指令给指纹模块
	ret = R301SendCommand(dev, CMD_DownChar, arg, 1, NULL, 0);
	if (ret != 0 ){
        R301SetNotify(dev, ret);
        //bio_print_error(_("Receive download command failed\n"));
		goto out;
	}

	// 发送count-1个数据包
	for(i=0; i < count - 1; i++) {
		ret = R301SendDataPackage(dev, &data[pkgsize * i], pkgsize);
		if(ret < 0){
			bio_print_error(_("Send data package failed\n"));
			goto out;
		}

	}
	j = datalength - pkgsize * (count - 1);						//计算结束包数据长度
	ret = R301SendFinish(dev, &data[pkgsize * (count - 1)], j);	//发送结束包
	if(ret < 0){
		bio_print_error(_("Send data package failed\n"));
		goto out;
	}

	return 0;

out:
	return ret;
}
/************************************************************************
函数名称： R301UpChar
功能说明： 上传缓冲区2中指纹特征到本地
传入参数： data(接收到的数据包以及结束包数据)
输出参数： int（成功返回接收到的数据长度，失败返回错误码）
************************************************************************/
int R301UpChar(bio_dev * dev, uint8 *data)
{
	int ret = 0;
	uint8 bagid = 0;
	uint16 length = 0;
	int i,j=0,count = 0;

	ret = R301SendCommand(dev, CMD_UpChar, NULL, 0, NULL, 0);
	if (ret !=0 ){
        R301SetNotify(dev, ret);
        //bio_print_error(_("Get fingerprint characteristics failed\n"));
		ret = -1;
		goto out;
	}

	do{
		memset(pack, 0, 10);
		ret = R301RecvData(dev, pack, 9);	//获取包头，地址，标识和长度
		if(ret < 9){
			bio_print_error(_("Get message header failed\n"));
			ret = -1;
			goto out;
		}

		bagid = pack[6];	//包标识
		length = (pack[7]<<8) + pack[8];	//包长度
		ret = R301RecvData(dev, bufferdata, length);		//包数据内容
		if(ret < length){
			bio_print_error(_("Get message failed\n"));
			ret = -1;
			goto out;
		}

		for(i=0; i<length-2; i++)
		{
			j = i + count;
			data[j] = bufferdata[i];
		}
		count = count + length - 2;

		if(bagid == BagID_Data)
			continue;
		else
			break;
	}while(1);

	return count;

out:
	return ret;
}

int R301SearchFingerInFlash(bio_dev * dev, int idx_start, int len, uint8 cmd)
{
	int ret = 0;
	uint8 arg[8];
	uint8 res[4];
	uint8 buffid = 1;

	arg[0] = buffid;					//缓冲区编号
	arg[1] = (idx_start & 0xff00) >> 8;	//搜索起始地址高字节
	arg[2] = idx_start & 0xff;			//搜索起始地址低字节
	arg[3] = (len & 0xff00) >> 8;		//搜索长度高字节
	arg[4] = len & 0xff;				//搜索长度低字节
	ret = R301SendCommand(dev, cmd, arg, 5, res, 4);

	if (ret !=0 ) {
        R301SetNotify(dev, ret);
        ret = -1;
	} else {
		ret = (res[0] << 8) + res[1];
	}

	return ret;
}

int R301DeleteFingerInFlash(bio_dev * dev, int start, int num)
{
	int ret = 0;
	uint8 arg[5];

	arg[0] = (start & 0xff00) >> 8;	//指纹地址高字节
	arg[1] = start & 0xff;			//指纹地址低字节
	arg[2] = (num & 0xff00) >> 8;	//删除个数高字节
	arg[3] = num & 0xff;			//删除个数低字节

	ret = R301SendCommand(dev, CMD_DeletChar, arg, 4, NULL, 0);
	if (ret != 0 )
	{
        R301SetNotify(dev, ret);
        //bio_print_error(_("Delete fingerprint failed\n"));
	}

	return ret;
}

int R301PortControl(bio_dev * dev, uint8 control)
{
	int ret = 0;
	uint8 arg[1];

	arg[0] = control;
	ret = R301SendCommand(dev, CMD_PortControl, arg, 1, NULL, 0);
	if (ret != 0 ){
        R301SetNotify(dev, ret);
        //bio_print_error(_("Set port for R301 failed\n"));
		return -1;
	}

	return ret;
}

int R301DeviceInit(bio_dev * dev, int Speed, int Bits, char Event, int Stop)
{
	int fd = 0;
	int ret = 0;

	/*打开指纹设备*/
	fd = open(dev->serial_info.path, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0){
		bio_print_error(_("Open R301 device failed\n"));
		return -1;
	}

	ret = fcntl(fd, F_SETFL, 0);
	if(ret < 0){
		bio_print_error(_("Set R301 port[%s] file descriptor flag failed\n"),
						dev->serial_info.path);
		close(fd);
		return ret;
	}

	ret = R301DeviceSet(fd, SERIAL_SPEED, SERIAL_BITS, SERIAL_EVENT, SERIAL_STOP);
	if (ret < 0){
		bio_print_error(_("Set R301 device failed\n"));
		close(fd);
		return ret;
	}

	dev->serial_info.fd = fd;

	return 0;
}

int R301DeviceExit(bio_dev * dev)
{
	int fd = dev->serial_info.fd;

	if (fd < 0){
		bio_print_error(_("No R301 finger print device detected\n"));
		return -1;
	}

	R301PortControl(dev, Control_OFF);

	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &oldtio);

	dev->serial_info.fd = 0;

	return 0;
}

/****************************************************************************/

int ops_configure(bio_dev * dev, GKeyFile * conf)
{
	dev->driver_id = R301_ID;
	dev->device_name = "r301";
	dev->full_name = "GROW R301";
	dev->bioinfo.biotype = BioT_FingerPrint;
	dev->bioinfo.stotype = StoT_OS;
	dev->bioinfo.eigtype = EigT_Eigenvalue;
	dev->bioinfo.vertype = VerT_Hardware;
	dev->bioinfo.idtype = IdT_Hardware;
	dev->bioinfo.bustype = BusT_Serial;
	dev->serial_info.fd = 0;

	dev->ops_configure = ops_configure;
	dev->ops_driver_init = r301_ops_driver_init;
	dev->ops_discover = r301_ops_discover;
	dev->ops_open = r301_ops_open;
	dev->ops_enroll = r301_ops_enroll;
	dev->ops_verify = r301_ops_verify;
	dev->ops_identify = r301_ops_identify;
	dev->ops_capture = r301_ops_capture;
	dev->ops_search = r301_ops_search;
	dev->ops_clean = r301_ops_clean;
//	dev->ops_get_feature_list = r301_ops_get_feature_list;
	dev->ops_get_feature_list = NULL;
	dev->ops_attach = r301_ops_attach;
	dev->ops_detach = r301_ops_detach;
	dev->ops_stop_by_user = r301_ops_stop_by_user;
	dev->ops_close = r301_ops_close;
	dev->ops_free = r301_ops_free;
	dev->ops_get_ops_result_mesg = r301_ops_get_ops_result_mesg;
	dev->ops_get_dev_status_mesg = r301_ops_get_dev_status_mesg;
	dev->ops_get_notify_mid_mesg = r301_ops_get_notify_mid_mesg;

	bio_set_drv_api_version(dev);
	bio_set_dev_status(dev, DEVS_COMM_IDLE);
	bio_set_ops_result(dev, OPS_COMM_SUCCESS);
	bio_set_notify_mid(dev, NOTIFY_COMM_IDLE);

	r301_timeout = bio_get_ops_timeout_ms();

	if (bio_dev_set_serial_path(dev, conf) != 0)
		return -1;
	dev->enable = bio_dev_is_enable(dev, conf);
	return 0;
}

const char * r301_ops_get_dev_status_mesg(bio_dev *dev)
{
	return NULL;
}

const char * r301_ops_get_ops_result_mesg(bio_dev *dev)
{
    switch (bio_get_ops_result(dev)) {

    case GET_FP_ERROR:
        return _("Collect fingerprint characteristics error");
    case GET_FP_NEED_RELEASE:
        return _("Fingerprint characteristics collected, please raise your finger");
    case GET_FP_NEED_PUSH:
        return _("Collecting fingerprint characteristics, please press you finger");
    case GET_FP_NEED_PUSH_2:
        return _("For the 2nd collection fingerprint characteristics, "
                 "please press you finger");
    case GET_FP_NEED_PUSH_3:
        return _("For the 3rd collection fingerprint characteristics, "
                 "please press you finger");
    case GET_FP_NEED_PUSH_4:
        return _("For the 4th collection fingerprint characteristics, "
                 "please press you finger");
    case GET_FP_NEED_PUSH_5:
        return _("For the 5th collection fingerprint characteristics, "
                 "please press you finger");
    case GET_FP_KEEP_PUSH:
        return _("Collecting fingerprint characteristics, "
                 "please keep your finger pressed");
    case CMD_GENCHAR_ERROR:
        return _("Generate fingerprint characteristics failed");
    case CMD_DOWNCHAR_ERROR:
        return _("Transport fingerprint template failed");
    case CMD_STORECHAR_ERROR:
        return _("Save fingerprint template failed");
    case FLASH_FULL_ERROR:
        return _("R301 device has insufficient storage space to record the fingerprint");
    case SAVE_FP_MODEL:
        return _("The fingerprint template is being saved, please wait");

    case DATA_GET_ERROR:
        return _("Packet acceptance error");
    case CHECK_NO_FINGER:
        return _("There are no fingers on the sensor");
    case ENROLL_IMAGE_ERROR:
        return _("Failed to input fingerprint image");
    case FINGER_IMAGE_DRY:
        return _("Fingerprint image is too dry and too light to be characteristic");
    case FINGER_IMAGE_BURNT:
        return _("Fingerprint image is too wet and too paste to produce features");
    case FINGER_IMAGE_MIXED:
        return _("The fingerprint image is too messy to be characteristic");
    case FINGER_IMAGE_POOR:
        return _("There are too few feature points to generate features");
    case VERIFY_NO_MATCH:
        return _("Fingerprint mismatch");
    case SEARCH_NO_MATCH:
        return _("No fingerprints were found");
    case CMD_REGMODEL_ERROR:
        return _("Generate fingerprint template failed");
    case ACCESS_OVER_RANGE:
        return _("The address serial number is out of the range of the fingerprint database");
    case READE_TEMPLATE_ERROR:
        return _("Error or failure of reading template");
    case PUSH_FEATURE_ERROR:
        return _("Failed to upload feature");
    case CMD_DATA_ERROR:
        return _("The module cannot accept subsequent packets");
    case PUSH_IMAGE_ERROR:
        return _("Failed to upload image");
    case CLEAN_TEMPLATE_ERROR:
        return _("Failed to delete template");
    case CLEAN_LIB_ERROR:
        return _("Failed to empty fingerprint Library");
    case CORE_ERROR:
        return _("password is incorrect");
    case BUFFER_NO_IMAGE:
        return _("Buffer has no valid original graph");
    case RW_FLASH_ERROR:
        return _("Error reading and writing flash");
    case UNDEFINED_ERROR:
        return _("Undefined error");
    case INVALID_REGISTER_NUMBER:
        return _("Invalid register number");
    case REGISTER_CONTENT_ERROR:
        return _("Register setting content error");
    case NOTEPAD_PAGE_ERROR:
        return _("Notepad page number assignment error");
    case PROT_OPERATION_FAILED:
        return _("Port operation failed");
    case ENROLL_FAILED:
        return _("enroll error");
    case FINGER_LIB_FULL:
        return _("Fingerprint library full");
    case RESERVED:
        return _("RESERVED");
    case COM_ERROR:
        return _("common error");

    default:
        return NULL;
    }
}

const char * r301_ops_get_notify_mid_mesg(bio_dev *dev)
{
	switch (bio_get_notify_mid(dev)) {

	case GET_FP_ERROR:
		return _("Collect fingerprint characteristics error");
	case GET_FP_NEED_RELEASE:
		return _("Fingerprint characteristics collected, please raise your finger");
	case GET_FP_NEED_PUSH:
		return _("Collecting fingerprint characteristics, please press you finger");
	case GET_FP_NEED_PUSH_2:
		return _("For the 2nd collection fingerprint characteristics, "
				 "please press you finger");
	case GET_FP_NEED_PUSH_3:
		return _("For the 3rd collection fingerprint characteristics, "
				 "please press you finger");
	case GET_FP_NEED_PUSH_4:
		return _("For the 4th collection fingerprint characteristics, "
				 "please press you finger");
	case GET_FP_NEED_PUSH_5:
		return _("For the 5th collection fingerprint characteristics, "
				 "please press you finger");
	case GET_FP_KEEP_PUSH:
		return _("Collecting fingerprint characteristics, "
				 "please keep your finger pressed");
	case CMD_GENCHAR_ERROR:
		return _("Generate fingerprint characteristics failed");
	case CMD_DOWNCHAR_ERROR:
		return _("Transport fingerprint template failed");
	case CMD_STORECHAR_ERROR:
		return _("Save fingerprint template failed");
	case FLASH_FULL_ERROR:
		return _("R301 device has insufficient storage space to record the fingerprint");
	case SAVE_FP_MODEL:
		return _("The fingerprint template is being saved, please wait");

    case DATA_GET_ERROR:
        return _("Packet acceptance error");
    case CHECK_NO_FINGER:
        return _("There are no fingers on the sensor");
    case ENROLL_IMAGE_ERROR:
        return _("Failed to input fingerprint image");
    case FINGER_IMAGE_DRY:
        return _("Fingerprint image is too dry and too light to be characteristic");
    case FINGER_IMAGE_BURNT:
        return _("Fingerprint image is too wet and too paste to produce features");
    case FINGER_IMAGE_MIXED:
        return _("The fingerprint image is too messy to be characteristic");
    case FINGER_IMAGE_POOR:
        return _("There are too few feature points to generate features");
    case VERIFY_NO_MATCH:
        return _("Fingerprint mismatch");
    case SEARCH_NO_MATCH:
        return _("No fingerprints were found");
    case CMD_REGMODEL_ERROR:
        return _("Generate fingerprint template failed");
    case ACCESS_OVER_RANGE:
        return _("The address serial number is out of the range of the fingerprint database");
    case READE_TEMPLATE_ERROR:
        return _("Error or failure of reading template");
    case PUSH_FEATURE_ERROR:
        return _("Failed to upload feature");
    case CMD_DATA_ERROR:
        return _("The module cannot accept subsequent packets");
    case PUSH_IMAGE_ERROR:
        return _("Failed to upload image");
    case CLEAN_TEMPLATE_ERROR:
        return _("Failed to delete template");
    case CLEAN_LIB_ERROR:
        return _("Failed to empty fingerprint Library");
    case CORE_ERROR:
        return _("password is incorrect");
    case BUFFER_NO_IMAGE:
        return _("Buffer has no valid original graph");
    case RW_FLASH_ERROR:
        return _("Error reading and writing flash");
    case UNDEFINED_ERROR:
        return _("Undefined error");
    case INVALID_REGISTER_NUMBER:
        return _("Invalid register number");
    case REGISTER_CONTENT_ERROR:
        return _("Register setting content error");
    case NOTEPAD_PAGE_ERROR:
        return _("Notepad page number assignment error");
    case PROT_OPERATION_FAILED:
        return _("Port operation failed");
    case ENROLL_FAILED:
        return _("enroll error");
    case FINGER_LIB_FULL:
        return _("Fingerprint library full");
    case RESERVED:
        return _("RESERVED");
    case COM_ERROR:
        return _("common error");

	default:
		return NULL;
	}
}

int r301_ops_driver_init(bio_dev *dev)
{
	int ret = 0;
	ret = access(dev->serial_info.path, F_OK | R_OK | W_OK);
	return ret;
}

int r301_ops_discover(bio_dev *dev)
{
	int ret = 0;

	ret = access(dev->serial_info.path, F_OK | R_OK | W_OK);
	if ( ret != 0)
	{
		bio_print_debug(_("Cannot operate device node: %s\n"), dev->serial_info.path);

		return -1;
	}

	/*打开指纹设备*/
	dev->serial_info.fd = open(dev->serial_info.path, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (dev->serial_info.fd < 0){
		bio_print_debug(_("Open R301 device failed\n"));
		dev->serial_info.fd = 0;

		return -1;
	}

	ret = fcntl(dev->serial_info.fd, F_SETFL, 0);
	if(ret < 0){
		bio_print_debug(_("Set R301 port[%s] file descriptor flag failed\n"),
						dev->serial_info.path);
		close(dev->serial_info.fd);
		dev->serial_info.fd = 0;

		return -1;
	}

//	ret = R301DeviceSet(fd, SERIAL_SPEED, SERIAL_BITS, SERIAL_EVENT, SERIAL_STOP);
	ret = R301DeviceSet(dev->serial_info.fd, 57600, 8, 'N', 1);
	if (ret < 0){
		bio_print_debug(_("Set R301 device failed\n"));
		tcflush(dev->serial_info.fd, TCIFLUSH);
		tcsetattr(dev->serial_info.fd, TCSANOW, &oldtio);
		close(dev->serial_info.fd);
		dev->serial_info.fd = 0;

		return -1;
	}

	uint8 para[16];
	ret = R301ReadSysPara(dev, para);
	if (ret != 0)
	{
		bio_print_debug(_("Get R301 device parameter failed\n"));
		tcflush(dev->serial_info.fd, TCIFLUSH);
		tcsetattr(dev->serial_info.fd, TCSANOW, &oldtio);
		close(dev->serial_info.fd);
		dev->serial_info.fd = 0;

		return -1;
	}


	R301PortControl(dev, Control_OFF);

	tcflush(dev->serial_info.fd, TCIFLUSH);
	tcsetattr(dev->serial_info.fd, TCSANOW, &oldtio);

	close(dev->serial_info.fd);
	dev->serial_info.fd = 0;

	return 1;
}

// 打开设备，成功返回0，失败返回-1
int r301_ops_open(bio_dev *dev)
{
	int ret = 0;

	bio_set_dev_status(dev, DEVS_COMM_IDLE);
	bio_set_ops_result(dev, OPS_COMM_SUCCESS);
	bio_set_notify_mid(dev, NOTIFY_COMM_IDLE);

	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		return -1;
	}
	bio_set_dev_status(dev, DEVS_OPEN_DOING);

	ret = R301DeviceInit(dev, 57600, 8, 'N', 1);
	if (ret != 0) {
		bio_set_ops_abs_result(dev, OPS_OPEN_ERROR);
		bio_set_notify_abs_mid(dev, NOTIFY_OPEN_ERROR);
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return -1;
	}

	/* 分配指纹特征数据数组 */
	int i = 0;
	for (i = 0; i < SAMPLE_TIMES; i++) {
		fp_data[i] = malloc(FP_SIZE);
		fp_base64[i] = malloc(FP_BASE64_SIZE);

		memset(fp_data[i], 0, FP_SIZE);
		memset(fp_base64[i], 0, FP_BASE64_SIZE);

		fp_data_len[i] = 0;
	}

	bio_set_ops_abs_result(dev, OPS_OPEN_SUCCESS);
	bio_set_dev_status(dev, DEVS_COMM_IDLE);

	return 0;
}

/**
 * @brief	获得指静脉信息
 * @param[in,out]	unsigned char* fvData	存放指静脉信息的buffer
 * @param[in]		unsigned int size		buffer大小
 * @param[in]		unsigned int* len		数据长度
 * @param[in]		unsigned int g_num		采集次数
 * @param[in]		unsigned int interval	时间间隔，毫秒
 * @exception
 * @return	OpsStatus	操作状态
 * @retval	OPS_COMM_SUCCESS(0)	成功
 *			其他					错误
 * @note	fvData一般是2048大小;
 *			循环判断g_num次，每次等待时间interval(ms)
 */
int R301GetFingerPrintData(bio_dev *dev, unsigned int sample_times, unsigned int interval)
{
	int ret = -1;
	unsigned int num = 0;

	int i;
	uint8 arg[5];

	R301OpsTimeusedReset();

	bio_set_notify_abs_mid(dev, GET_FP_NEED_PUSH);
	bio_print_info("%s\n", r301_ops_get_notify_mid_mesg(dev));
	for (num = 0; num < sample_times; num++) {
		bio_set_notify_abs_mid(dev, GET_FP_NEED_PUSH + num);
		bio_print_info("%s\n", dev->ops_get_notify_mid_mesg(dev));

		R301OpsTimeusedReset();
		for (i = 1; i <= 2; i++) {
			// 按下手指，获取指纹图像，合成指纹特征
			do {
				arg[0] = i;
				ret = -1;

				while (ret != 0) {
					if (R301OpsIsTimeout()) {
						bio_set_ops_result(dev, OPS_COMM_TIMEOUT);
						bio_set_notify_mid(dev, NOTIFY_COMM_TIMEOUT);
						bio_print_error("[%d] %s\n", bio_get_notify_mid(dev),
										bio_get_notify_mid_mesg(dev));
						return OPS_COMM_TIMEOUT;
					}

					if (bio_is_stop_by_user(dev)) {
						bio_set_notify_mid(dev, NOTIFY_COMM_STOP_BY_USER);
						bio_print_error("%s\n", bio_get_notify_mid_mesg(dev));
						return OPS_COMM_STOP_BY_USER;
					}

					ret = R301SendCommand(dev, CMD_GetImage, NULL, 0, NULL, 0);
					R301OpsElapsedTime(ELAPSED_TIME_MS);
					R301OpsTimeusedSleepMS(interval);
					if (ret !=0 ){
						continue;
					}
					bio_set_notify_abs_mid(dev, GET_FP_KEEP_PUSH);
				}

				ret = R301SendCommand(dev, CMD_GenChar, arg, 1, NULL, 0);
				if (ret !=0 ) {
//					bio_set_ops_result(dev, OPS_COMM_ERROR);
//					bio_set_notify_abs_mid(dev, CMD_GENCHAR_ERROR);
//					bio_print_error("%s (%d:%d).\n", bio_get_notify_mid_mesg(dev),
//						   ret, fp_data_len[num]);
                    R301SetNotify(dev, ret);
					return OPS_COMM_ERROR;
				}

			} while(ret != 0); // do
		} // for (i = 1; i <= 2; i++)

		// 抬起手指检测
		bio_set_notify_abs_mid(dev, GET_FP_NEED_RELEASE);
		bio_print_info("%s\n", bio_get_notify_mid_mesg(dev));
		ret = 0;
		R301OpsTimeusedReset();
		while ((ret == 0) && (num + 1 < sample_times)) {
			if (R301OpsIsTimeout()) {
				bio_set_ops_result(dev, OPS_COMM_TIMEOUT);
				bio_set_notify_mid(dev, NOTIFY_COMM_TIMEOUT);
				bio_print_error("%s\n", bio_get_notify_mid_mesg(dev));
				return OPS_COMM_TIMEOUT;
			}

			if (bio_is_stop_by_user(dev)) {
				bio_set_notify_mid(dev, NOTIFY_COMM_STOP_BY_USER);
				bio_print_error("%s\n", bio_get_notify_mid_mesg(dev));
				return OPS_COMM_STOP_BY_USER;
			}

			ret = R301SendCommand(dev, CMD_GetImage, NULL, 0, NULL, 0);
			R301OpsElapsedTime(ELAPSED_TIME_MS);
			R301OpsTimeusedSleepMS(interval);
			if (ret == 0) {
				continue;
			}
		}

		ret = R301SendCommand(dev, CMD_RegModel, NULL, 0, NULL, 0);
		if (ret != 0 ){
//			bio_set_ops_result(dev, OPS_COMM_ERROR);
//			bio_set_notify_abs_mid(dev, CMD_REGMODEL_ERROR);
//			bio_print_error("%s (%d:%d).\n", bio_get_notify_mid_mesg(dev),
//							ret, fp_data_len[num]);
            R301SetNotify(dev, ret);
			return OPS_COMM_ERROR;
		}

		fp_data_len[num] = R301UpChar(dev, fp_data[num]);
		if (fp_data_len[num] < 0) {
			bio_set_ops_result(dev, OPS_COMM_ERROR);
			bio_set_notify_abs_mid(dev, GET_FP_ERROR);
			bio_print_error("%s (%d:%d).\n", bio_get_notify_mid_mesg(dev),
							ret, fp_data_len[num]);
			return OPS_COMM_ERROR;
		}
		R301OpsTimeusedSleepMS(UPDOWN_INTERVAL_MS);

		bio_base64_encode(fp_data[num], fp_base64[num], fp_data_len[num]);
	}

	return OPS_COMM_SUCCESS;
}

void R301SetNotify(bio_dev *dev, int code)
{
    int notify = -1;

    switch (code) {
    case 1:
        notify = DATA_GET_ERROR;
        break;
    case 2:
        notify = CHECK_NO_FINGER;
        break;
    case 3:
        notify = ENROLL_IMAGE_ERROR;
        break;
    case 4:
        notify = FINGER_IMAGE_DRY;
        break;
    case 5:
        notify = FINGER_IMAGE_BURNT;
        break;
    case 6:
        notify = FINGER_IMAGE_MIXED;
        break;
    case 7:
        notify = FINGER_IMAGE_POOR;
        break;
    case 8:
        notify = VERIFY_NO_MATCH;
        break;
    case 9:
        notify = SEARCH_NO_MATCH;
        break;
    case 10:
        notify = CMD_REGMODEL_ERROR;
        break;
    case 11:
        notify = ACCESS_OVER_RANGE;
        break;
    case 12:
        notify = READE_TEMPLATE_ERROR;
        break;
    case 13:
        notify = PUSH_FEATURE_ERROR;
        break;
    case 14:
        notify = CMD_DATA_ERROR;
        break;
    case 15:
        notify = PUSH_IMAGE_ERROR;
        break;
    case 16:
        notify = CLEAN_TEMPLATE_ERROR;
        break;
    case 17:
        notify = CLEAN_LIB_ERROR;
        break;
    case 18:
        notify = CORE_ERROR;
        break;
    case 19:
        notify = BUFFER_NO_IMAGE;
        break;
    case 20:
        notify = RW_FLASH_ERROR;
        break;
    case 21:
        notify = UNDEFINED_ERROR;
        break;
    case 22:
        notify = INVALID_REGISTER_NUMBER;
        break;
    case 23:
        notify = REGISTER_CONTENT_ERROR;
        break;
    case 24:
        notify = NOTEPAD_PAGE_ERROR;
        break;
    case 25:
        notify = PROT_OPERATION_FAILED;
        break;
    case 26:
        notify = ENROLL_FAILED;
        break;
    case 27:
        notify = FINGER_LIB_FULL;
        break;
    case 28:
        notify = RESERVED;
        break;
    default:
        notify = COM_ERROR;
    }
    bio_set_ops_abs_result(dev, notify);
    bio_set_notify_abs_mid(dev, notify);
    bio_print_error("%s (%d).\n", bio_get_notify_mid_mesg(dev), code);
}

int R301GetEmptyIndex(bio_dev *dev, uint8 fp_idx[FP_MAX_INDEX])
{
	sqlite3 *db = bio_sto_connect_db();
	feature_info * info, * info_list;
	int i = 0;
	int count = 0;

	for (i = 0; i < FP_MAX_INDEX; i++) {
		fp_idx[i] = 0;
	}

	info_list = bio_sto_get_feature_info(db, -1, dev->bioinfo.biotype,
												 dev->device_name, 0,
												 -1);
	bio_sto_disconnect_db(db);
//	print_feature_info(info_list);

	info = info_list;
	while (info != NULL) {
		feature_sample * sample = info->sample;
		while (sample != NULL) {
			fp_idx[sample->no] = 1;
			sample = sample->next;
		}
		info = info->next;
	}
	bio_sto_free_feature_info_list(info_list);

	for (i = FP_MIN_INDEX; i < FP_MAX_INDEX; i++)
		if (fp_idx[i] == 0)
			count ++;

	return count;
}

int r301_ops_enroll(bio_dev *dev, OpsActions action, int uid, int bio_idx,
					 char * bio_idx_name)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
		return -1;
	}

	if (action == ACTION_START) {
		bio_set_dev_status(dev, DEVS_ENROLL_DOING);
		R301OpsTimeusedReset();

		uint8 fp_idx[FP_MAX_INDEX];
		int count = 0;
		count = R301GetEmptyIndex(dev, fp_idx);
		if (count < SAMPLE_TIMES) {
			bio_set_ops_result(dev, OPS_COMM_ERROR);
			bio_set_notify_abs_mid(dev, FLASH_FULL_ERROR);
			bio_set_dev_status(dev, DEVS_COMM_IDLE);
			return -1;
		}

		int ret = R301GetFingerPrintData(dev, SAMPLE_TIMES, INTERVAL);
		if (ret != OPS_COMM_SUCCESS) {
			bio_print_error(_("R301GetFingerPrintData error: %s\n"),
							bio_get_notify_mid_mesg(dev));
//			bio_set_ops_result(dev, ret);
			bio_set_dev_status(dev, DEVS_COMM_IDLE);
			return -1;
		}

		int i = 0;
		for (i = 0; i < SAMPLE_TIMES; i++) {
			bio_print_info(_("The %dth sampling is successful and the eigenvalue(Base64) "
							 "is: %.*s\n"), i + 1, 16, fp_base64[i]);
		}

		int empty_idx = 15;
		feature_info * info;
		feature_sample * sample, * head;
		info = bio_sto_new_feature_info(uid, dev->bioinfo.biotype,
												dev->device_name, bio_idx,
												bio_idx_name);
		head = bio_sto_new_feature_sample(-1, NULL);
		sample = head;

		R301OpsTimeusedReset();
		bio_set_notify_abs_mid(dev, SAVE_FP_MODEL);
		for (i = 0; i < SAMPLE_TIMES; i++) {
			int j = 0;
			for (j = empty_idx; j < FP_MAX_INDEX; j++) {
				if (fp_idx[j] == 0) {
					empty_idx = j;
					break;
				}
			}

			ret = R301DownChar(dev, fp_data[i], fp_data_len[i]);
			if (ret != 0 ) {
                bio_set_ops_result(dev, OPS_COMM_ERROR);
                bio_set_notify_abs_mid(dev, CMD_DOWNCHAR_ERROR);
				bio_set_dev_status(dev, DEVS_COMM_IDLE);
				return -1;
			}
			R301OpsTimeusedSleepMS(UPDOWN_INTERVAL_MS);

			uint8 arg[3];
			arg[0] = 2;
			arg[1] = (empty_idx & 0xff00) >> 8;
			arg[2] = empty_idx & 0xff;
			ret = R301SendCommand(dev, CMD_StoreChar, arg, 3, NULL, 0);
			if (ret != 0 ) {
//				bio_set_ops_result(dev, OPS_COMM_ERROR);
//				bio_set_notify_abs_mid(dev, CMD_STORECHAR_ERROR);
//				bio_set_dev_status(dev, DEVS_COMM_IDLE);
                R301SetNotify(dev, ret);
				return -1;
			}
			fp_idx[empty_idx] = 1;

			feature_sample * s = bio_sto_new_feature_sample(-1, NULL);
			s->no = empty_idx;
			s->data = bio_sto_new_str(fp_base64[i]);
			s->next = NULL;
			sample->next = s;
			sample = sample->next;

		}

		info->sample = head->next;
		bio_sto_free_feature_sample(head);

//		print_feature_info(info);

		sqlite3 *db = bio_sto_connect_db();
		bio_sto_set_feature_info(db, info);
		bio_sto_disconnect_db(db);

		bio_sto_free_feature_info_list(info);

		bio_set_ops_abs_result(dev, OPS_ENROLL_SUCCESS);
		bio_set_notify_abs_mid(dev, NOTIFY_ENROLL_SUCCESS);
	} else {
		bio_set_ops_abs_result(dev, OPS_ENROLL_STOP_BY_USER);
		bio_set_notify_abs_mid(dev, NOTIFY_ENROLL_STOP_BY_USER);
	}

	bio_set_dev_status(dev, DEVS_COMM_IDLE);
	return 0;
}

char * r301_ops_capture(bio_dev *dev, OpsActions action)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		return NULL;
	}

	if (action == ACTION_START) {
		bio_set_dev_status(dev, DEVS_CAPTURE_DOING);
		R301OpsTimeusedReset();

		int ret = R301GetFingerPrintData(dev, 1, INTERVAL);
		if (ret != 0) {
			bio_print_error(_("R301GetFingerPrintData error: %s\n"),
							bio_get_notify_mid_mesg(dev));
			bio_set_ops_abs_result(dev, OPS_CAPTURE_ERROR);
			bio_set_dev_status(dev, DEVS_COMM_IDLE);
			return NULL;
		}

		if (capture_base64 != NULL)
			free(capture_base64);
		capture_base64 = bio_sto_new_str(fp_base64[0]);

		bio_set_ops_abs_result(dev, OPS_CAPTURE_SUCCESS);
		bio_set_notify_abs_mid(dev, NOTIFY_CAPTURE_SUCCESS);
	} else {
		if (capture_base64 != NULL)
			free(capture_base64);
		capture_base64 = NULL;
		bio_set_ops_abs_result(dev, OPS_CAPTURE_STOP_BY_USER);
		bio_set_notify_abs_mid(dev, NOTIFY_CAPTURE_STOP_BY_USER);
	}

	bio_set_dev_status(dev, DEVS_COMM_IDLE);
	return capture_base64;
}

/**
 * @brief r301_fp_find
 * 当前录入指静脉信息与数据库中信息逐个匹配
 * @param[in] bio_dev * dev		设备结构体
 * @param[in] int uid			指定用户ID，-1代表匹配所有用户
 * @param[in] int bio_idx_start	指定开始的索引
 * @param[in] int bio_idx_end	指定结束的索引，-1代表到最后
 * @return feature_info *	匹配结果链表
 * @retval	NULL	未匹配到
 */
feature_info * r301_fp_find(bio_dev *dev, int uid, int idx_start, int idx_end)
{
	feature_info * found = NULL;
	int ret = R301GetFingerPrintData(dev, 1, INTERVAL);
	if (ret != 0) {
		bio_print_error(_("R301GetFingerPrintData error: %s\n"),
				bio_get_notify_mid_mesg(dev));
		bio_set_ops_result(dev, OPS_COMM_ERROR);
		return NULL;
	}

	int find_list[FP_MAX_INDEX + 1];
	int find_idx = FP_MIN_INDEX - 1;
	int i = 0;

	for (i = 0; i <= FP_MAX_INDEX; i++)
		find_list[i] = 0;

	GString * print_list = g_string_new(NULL);
	g_string_append(print_list, _("Matched ID:"));
	while (find_idx > 0) {
		find_idx = R301SearchFingerInFlash(dev, find_idx + 1, FP_MAX_INDEX - find_idx, CMD_Search);
		if (find_idx > 0) {
			find_list[find_idx] = 1;
			g_string_sprintfa(print_list, " %d", find_idx);
		}
	}
	bio_print_info("%s\n", print_list->str);
	g_string_free(print_list, TRUE);

	sqlite3 *db = bio_sto_connect_db();
	feature_info * info, * info_list;

	info_list = bio_sto_get_feature_info(db, uid, dev->bioinfo.biotype,
												 dev->device_name, idx_start,
												 idx_end);
//	print_feature_info(info_list);
	bio_sto_disconnect_db(db);

	ret = 0;
	info = info_list;

	while (info != NULL) {
		feature_sample * sample = info->sample;
		while (sample != NULL) {
			if (find_list[sample->no] == 1) {
				found = bio_sto_new_feature_info(info->uid, info->biotype,
												 info->driver, info->index,
												 info->index_name);
				found->sample = bio_sto_new_feature_sample(sample->no, sample->data);
				break;
			}
			sample = sample->next;
		}
		info = info->next;
		if (found != NULL)
			break;
	}
	bio_sto_free_feature_info_list(info_list);

	return found;
}

int r301_ops_verify(bio_dev *dev, OpsActions action, int uid, int idx)
{
	feature_info * found;
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		return -1;
	}

	if (action == ACTION_START) {
		bio_set_dev_status(dev, DEVS_VERIFY_DOING);
		R301OpsTimeusedReset();

		int found_idx = -1;
		found = r301_fp_find(dev, uid, idx, idx);
		if (found != NULL) {
			bio_set_ops_abs_result(dev, OPS_VERIFY_MATCH);
			bio_set_notify_abs_mid(dev, NOTIFY_VERIFY_MATCH);
			found_idx = found->index;
			bio_sto_free_feature_info(found);
		} else {
			bio_set_ops_abs_result(dev, OPS_VERIFY_NO_MATCH);
			bio_set_notify_abs_mid(dev, NOTIFY_VERIFY_NO_MATCH);
		}

		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return found_idx;
	} else {
		bio_set_ops_abs_result(dev, OPS_VERIFY_STOP_BY_USER);
		bio_set_notify_abs_mid(dev, NOTIFY_VERIFY_STOP_BY_USER);
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return -1;
	}
}

int r301_ops_identify(bio_dev *dev, OpsActions action, int uid,
					   int idx_start, int idx_end)
{
	feature_info * found;
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev,DEVS_COMM_DISABLE);
		return -1;
	}

	if (action == ACTION_START) {
		bio_set_dev_status(dev, DEVS_IDENTIFY_DOING);
		R301OpsTimeusedReset();

		int found_uid = -1;
		found = r301_fp_find(dev, uid, idx_start, idx_end);
		if (found != NULL) {
			bio_set_ops_abs_result(dev, OPS_IDENTIFY_MATCH);
			bio_set_notify_abs_mid(dev, NOTIFY_IDENTIFY_MATCH);
			found_uid = found->uid;
			bio_sto_free_feature_info(found);
		} else {
			bio_set_ops_abs_result(dev, OPS_IDENTIFY_NO_MATCH);
			bio_set_notify_abs_mid(dev, NOTIFY_IDENTIFY_NO_MATCH);
		}

		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return found_uid;
	} else {
		bio_set_ops_abs_result(dev, OPS_IDENTIFY_STOP_BY_USER);
		bio_set_notify_abs_mid(dev, NOTIFY_IDENTIFY_STOP_BY_USER);
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return -1;
	}
}

feature_info * r301_ops_search(bio_dev *dev, OpsActions action, int uid,
										int idx_start, int idx_end)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		return NULL;
	}

	if (action == ACTION_START) {
		bio_set_dev_status(dev, DEVS_SEARCH_DOING);
		R301OpsTimeusedReset();

		feature_info * found = NULL;
		found = r301_fp_find(dev, uid, idx_start, idx_end);
		if (found != NULL) {
			bio_set_ops_abs_result(dev, OPS_SEARCH_MATCH);
			bio_set_notify_abs_mid(dev, NOTIFY_SEARCH_MATCH);
		} else {
			bio_set_ops_abs_result(dev, OPS_SEARCH_NO_MATCH);
			bio_set_notify_abs_mid(dev, NOTIFY_SEARCH_NO_MATCH);
		}

		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return found;
	} else {
		bio_set_ops_abs_result(dev, OPS_SEARCH_STOP_BY_USER);
		bio_set_notify_abs_mid(dev, NOTIFY_SEARCH_STOP_BY_USER);
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return NULL;
	}
}

int r301_ops_clean(bio_dev *dev, OpsActions action, int uid,
					int idx_start, int idx_end)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		return 0;
	}

	if (action == ACTION_START) {
		bio_set_dev_status(dev, DEVS_CLEAN_DOING);
		R301OpsTimeusedReset();

		sqlite3 *db = bio_sto_connect_db();
		feature_info * info, * info_list;
		int ret = 0;
		int i = 0;
		int clean_list[FP_MAX_INDEX + 1];

		info_list = bio_sto_get_feature_info(db, uid, dev->bioinfo.biotype,
													 dev->device_name, idx_start,
													 idx_end);
//		print_feature_info(info_list);

		for (i = 0; i <= FP_MAX_INDEX; i++)
			clean_list[i] = 0;

		ret = 0;
		info = info_list;
		while (info != NULL) {
			feature_sample * sample = info->sample;
			while (sample != NULL) {
				clean_list[sample->no] = 1;
				sample = sample->next;
			}
			info = info->next;
		}
		bio_sto_free_feature_info_list(info_list);

		int start = FP_MIN_INDEX;
		int count = 0;

		while (start <= FP_MAX_INDEX) {
			count = 0;
			while (clean_list[start + count] == 1) {
				count++;
			}
			if (count != 0) {
				R301DeleteFingerInFlash(dev, start, count);
			}
			start = start + count + 1;
		}

		ret = bio_sto_clean_feature_info(db, uid, dev->bioinfo.biotype,
												 dev->device_name, idx_start,
												 idx_end);
		bio_sto_disconnect_db(db);
        if (0 == ret) {
			bio_set_ops_abs_result(dev, OPS_CLEAN_SUCCESS);
			bio_set_notify_abs_mid(dev, NOTIFY_CLEAN_SUCCESS);
		} else {
			bio_set_ops_abs_result(dev, OPS_CLEAN_FAIL);
			bio_set_notify_abs_mid(dev, NOTIFY_CLEAN_FAIL);
		}

		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return ret;
	} else {
		bio_set_ops_abs_result(dev, OPS_CLEAN_STOP_BY_USER);
		bio_set_notify_abs_mid(dev, NOTIFY_CLEAN_STOP_BY_USER);
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return 0;
	}

}

feature_info * r301_ops_get_feature_list(bio_dev *dev, OpsActions action,
										  int uid, int idx_start, int idx_end)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		return NULL;
	}

	if (action == ACTION_START) {
		bio_set_dev_status(dev, DEVS_GET_FLIST_DOING);
		R301OpsTimeusedReset();
		usleep(100000);

		feature_info * finfo_list = NULL;
		sqlite3 *db = bio_sto_connect_db();
		finfo_list = bio_sto_get_feature_info(db, uid, dev->bioinfo.biotype,
													 dev->device_name, idx_start,
													 idx_end);
//		print_feature_info(finfo_list);
		bio_sto_disconnect_db(db);

		bio_set_ops_abs_result(dev, OPS_GET_FLIST_SUCCESS);
		bio_set_notify_abs_mid(dev, NOTIFY_GET_FLIST_SUCCESS);
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return finfo_list;
	} else {
		bio_set_ops_abs_result(dev, OPS_GET_FLIST_STOP_BY_USER);
		bio_set_notify_abs_mid(dev, NOTIFY_GET_FLIST_STOP_BY_USER);
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		return NULL;
	}
}

void r301_ops_close(bio_dev *dev)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
	}
	bio_set_dev_status(dev, DEVS_CLOSE_DOING);

	R301DeviceExit(dev);

	int i = 0;
	for (i = 0; i < SAMPLE_TIMES; i++) {
		if (fp_data[i] != NULL) {
			free(fp_data[i]);
			fp_data[i] = NULL;
		}
		if (fp_base64 != NULL) {
			free(fp_base64[i]);
			fp_base64[i] = NULL;
		}
	}
	if (capture_base64 != NULL) {
		free(capture_base64);
		capture_base64 = NULL;
	}

	bio_set_dev_status(dev, DEVS_COMM_IDLE);
}

void r301_ops_attach(bio_dev *dev)
{
	return;
}

void r301_ops_detach(bio_dev *dev)
{
	return;
}

int r301_ops_stop_by_user(bio_dev * dev, int waiting_ms)
{
	bio_print_info(_("Device %s[%d] received interrupt request\n"),
		   dev->device_name, dev->driver_id);
	int timeout = bio_get_ops_timeout_ms();
	int timeused = 0;
	int dev_status = bio_get_dev_status(dev);
    int ops_class = -1;
    ops_class = dev_status / 100;

	if (waiting_ms < timeout)
		timeout = waiting_ms;

	if (bio_get_dev_status(dev) % 100 != DEVS_COMM_IDLE)
		bio_set_dev_status(dev, ops_class * 100 + DEVS_COMM_STOP_BY_USER);
	while ((bio_get_dev_status(dev) % 100 != DEVS_COMM_IDLE) &&
		   (timeused < timeout)){
		timeused += INTERVAL;
		usleep(INTERVAL * 1000);
	}

	if (bio_get_dev_status(dev) % 100 == DEVS_COMM_IDLE)
		return 0;

	// 中断失败，还原操作状态
	bio_set_dev_status(dev, dev_status);
	return -1;
}

void r301_ops_free(bio_dev *dev)
{
	bio_print_info(_("R301 driver unload\n"));
}


