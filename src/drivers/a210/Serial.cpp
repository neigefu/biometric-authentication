#include "Serial.h"
#include "ComOperator.h"
#include <biometric_common.h>
#include <biometric_intl.h>

CSerial::CSerial(void)
{
	speed_arr[0] = B38400;
	speed_arr[1] = B19200;
	speed_arr[2] = B9600;
	speed_arr[3] = B4800;
	speed_arr[4] = B2400;
	speed_arr[5] = B1200;
	speed_arr[6] = B300;
	speed_arr[7] = B115200;

	name_arr[0] = 38400;
	name_arr[1] = 19200;
	name_arr[2] = 9600;
	name_arr[3] = 4800;
	name_arr[4] = 2400;
	name_arr[5] = 1200;
	name_arr[6] = 300;
	name_arr[7] = 115200;
	m_nReadlen = 0;

	NeedRunning = false;
	Timeout = 28;
	StopFlag = false;
}

CSerial::~CSerial(void)
{
	if (hSvrThread)
	{
		pthread_cancel(hSvrThread);
	}
	NeedRunning = false;
}

//设置波特率
void CSerial::set_speed(int speed)
{
	if (m_fd <= 0)
		return;

	int i;
	int status;
	int size = sizeof(speed_arr) / sizeof(int);
	struct termios Opt;
	tcgetattr(m_fd, &Opt);


	for (i = 0; i < size; i++)
	{
		if (speed == name_arr[i])
		{
			tcflush(m_fd, TCIOFLUSH);
			cfsetispeed(&Opt, speed_arr[i]);
			cfsetospeed(&Opt, speed_arr[i]);
			status = tcsetattr(m_fd, TCSANOW, &Opt);
			if (status != 0)
				bio_print_error("tcsetattr fd!\n");
			return;
		}
		tcflush(m_fd, TCIOFLUSH);
	}
}

/**
*  设置串口数据位，停止位和效验位
*  fd     类型  int  打开的串口文件句柄*
*  databits 类型  int 数据位   取值 为 7 或者8*
*  stopbits 类型  int 停止位   取值为 1 或者2*
*  parity  类型  int  效验类型 取值为N,E,O,,S
*/
int CSerial::set_Parity(int databits, int stopbits, char parity,int iSpeed)
{
	struct termios newtio;
	struct termios oldtio;

	if(tcgetattr(m_fd,&oldtio) != 0)
	{
		return -1;
	}

	bzero(&newtio,sizeof(newtio));
	newtio.c_cflag |= CLOCAL |CREAD;
	newtio.c_cflag &= ~CSIZE;

	switch(databits)
	{
		case 7:
			newtio.c_cflag |= CS7;
			break;
		case 8:
			newtio.c_cflag |= CS8;
			break;
	}

	switch(parity)
	{
		case 'O':
			newtio.c_cflag |= PARENB;
			newtio.c_cflag |= PARODD;
			newtio.c_iflag |= (INPCK | ISTRIP);
			break;
		case 'E':
			newtio.c_iflag |= (INPCK |ISTRIP);
			newtio.c_cflag |= PARENB;
			newtio.c_cflag &= ~PARODD;
			break;
		case 'N':
			newtio.c_cflag &= ~PARENB;
			break;
	}

	switch(iSpeed)
	{
		case 2400:
			cfsetispeed(&newtio,B2400);
			cfsetospeed(&newtio,B2400);
			break;
		case 4800:
			cfsetispeed(&newtio,B4800);
			cfsetospeed(&newtio,B4800);
			break;
		case 9600:
			cfsetispeed(&newtio,B9600);
			cfsetospeed(&newtio,B9600);
			break;
		case 19200:
			cfsetispeed(&newtio,B19200);
			cfsetospeed(&newtio,B19200);
			break;
		case 115200:
			cfsetispeed(&newtio,B115200);
			cfsetospeed(&newtio,B115200);
			break;
		case 460800:
			cfsetispeed(&newtio,B460800);
			cfsetospeed(&newtio,B460800);
			break;
		default:
			cfsetispeed(&newtio,B9600);
			cfsetospeed(&newtio,B9600);
			break;
	}

	if(stopbits == 1)
	{
		newtio.c_cflag &= ~CSTOPB;
	}
	else if(stopbits ==2)
	{
		newtio.c_cflag |= CSTOPB;
	}

	newtio.c_cc[VTIME] = 0;
	newtio.c_cc[VMIN] = 0;

	tcflush(m_fd,TCIOFLUSH);
	if((tcsetattr(m_fd,TCSANOW,&newtio)) != 0)
	{
		return -1;
	}

	return 0;
}

/**
* 打开串口
*/
int CSerial::OpenDev(char *Dev)
{
	m_fd = open(Dev, O_RDWR | O_NOCTTY | O_NDELAY);
	if (-1 == m_fd)
	{ /*设置数据位数*/
		bio_print_error(_("Can't Open Serial Port: %s\n"), Dev);
		return -SERIAL_ERROR;
	}

	return 0;
}

void CSerial::CloseDev()
{
	NeedRunning = false;
	close(m_fd);
}

int CSerial::StartMonitoring()
{

	if (hSvrThread > 0)
		return -1;

	int err = 0;

	memset(&hSvrThread, 0, sizeof(hSvrThread));

	NeedRunning = true;
	err = pthread_create(&hSvrThread, NULL, threadRead, this);
	if (err != 0)
	{
		bio_print_error("pthread_create failed!");
		return -1;
	}

	return 0;
}

void CSerial::SetParent(void *pParent)
{
	m_pParent = pParent;
}

int CSerial::WriteToPort(unsigned char *chData, int nlen)
{
	if (m_fd > 0)
	{   tcflush(m_fd,TCIFLUSH);
		return write(m_fd, (char *)chData, nlen);
	}

	return -1;
}

void *CSerial::threadRead(void *ptr)
{
	CSerial *pSerial = static_cast<CSerial *>(ptr);
	if (NULL == pSerial)
	{
		return (void *)(-1);
	}

	int fd = pSerial->m_fd;
	int nread;
	char Rxbuff[MAX_PACK_LEN];

	pSerial->NeedRunning = true;
	while(pSerial->NeedRunning)
	{
		memset(Rxbuff, 0, MAX_PACK_LEN);
		int nRecvlen = 0;
		while ((nread = read(fd, Rxbuff + nRecvlen, MAX_PACK_LEN)) > 0)
		{
			nRecvlen += nread;
			usleep(1000);
		}

		if (nRecvlen > 0 && nRecvlen < MAX_PACK_LEN)
		{
			CComOperator::RecveData(static_cast<CComOperator *>(pSerial->m_pParent), Rxbuff, nRecvlen);
			memset(Rxbuff, 0, MAX_PACK_LEN);
		}
	}

	return 0;
}

int CSerial::WaitingForRead(void *ptr)
{
#ifdef COM_OPERATOR_DEBUG
	bio_print_debug("WaitingForRead ...\n");
#endif

	CSerial *pSerial = static_cast<CSerial *>(ptr);

	if (NULL == pSerial)
	{
		return SERIAL_ERROR;
	}

	int fd = pSerial->m_fd;

	long timeout_ms = pSerial->Timeout * 1000;
	struct timeval previousTime, currentTime;
	long distance = 0;

	int nread;
	char Rxbuff[MAX_PACK_LEN];

	int ret = SERIAL_SUCCESS;

	pSerial->NeedRunning = true;
	gettimeofday (&previousTime, NULL);
	while((pSerial->NeedRunning))
	{
		if (pSerial->StopFlag)
		{
			ret = SERIAL_STOP_BY_USER;
			pSerial->StopFlag = false;

#ifdef COM_OPERATOR_DEBUG
			bio_print_info("In WaitingForRead, SERIAL_STOP_BY_USER\n");
#endif

			break;
		}

		if (distance > timeout_ms)
		{
			ret = SERIAL_TIMEOUT;
			break;
		}

		memset(Rxbuff, 0, MAX_PACK_LEN);
		int nRecvlen = 0;
		while ((nread = read(fd, Rxbuff + nRecvlen, MAX_PACK_LEN)) > 0)
		{
			nRecvlen += nread;
			usleep(1000);
		}

		if (nRecvlen > 0 && nRecvlen < MAX_PACK_LEN)
		{
			ret = CComOperator::RecveData(static_cast<CComOperator *>(pSerial->m_pParent), Rxbuff, nRecvlen);
			memset(Rxbuff, 0, MAX_PACK_LEN);

#ifdef COM_OPERATOR_DEBUG
			bio_print_debug("RecveData ret = %d\n", ret);
#endif

			if (ret != COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING)
				break;
		}

		gettimeofday (&currentTime, NULL);
		distance = (currentTime.tv_sec - previousTime.tv_sec) * 1000 +
				   (currentTime.tv_usec - previousTime.tv_usec) /1000;

		usleep(10 * 1000);
	}

#ifdef COM_OPERATOR_DEBUG
	bio_print_debug("WaitingForRead done, Ret = %d\n", ret);
#endif

	return ret;
}

void CSerial::SetReadlen(int ilen)
{
	m_nReadlen = ilen;
}

int CSerial::GetReadlen()
{
	return m_nReadlen;
}
