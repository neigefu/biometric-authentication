/*
 * Copyright (C) 2018 Tianjin KYLIN Information Technology Co., Ltd.
 *
 * Author: Droiing <jianglinxuan@kylinos.cn>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#ifndef A210_H
#define A210_H

#include <stdint.h>
#include <biometric_common.h>

#include "liba210.h"

#if	defined(__cplusplus)
extern	"C"	{
#endif	/* defined(__cplusplus) */

#define A210_DEBUG
//#define A210_DEBUG_LOW_LEVEL

#define A210_MIN_INDEX	0
#define A210_MAX_INDEX	50

#define TIME_INTERVAL 2
#define TIME_SAMPLE_TIMEOUT 25

#define NOTIFY_STRING_MAX_LEN 255

enum a210_notify_mid {
	A210_NOTIFY_BASE = (NOTIFY_COMM_MAX + 1) + (COM_OPS_RET_MAX + 1),
	A210_NOTIFY_INTERNAL_NOTIFY,
	A210_NOTIFY_INTERNAL_ERROR,
	A210_NOTIFY_SEE_MIRROR,
	A210_NOTIFY_ENROLL_STORAGE_FULL,
};


// 配置驱动信息
int ops_configure(bio_dev * dev, GKeyFile * conf);

// 驱动初始化设置函数
int a210_ops_driver_init(bio_dev *dev);

// 驱动发现函数
int a210_ops_discover(bio_dev *dev);

// 打开设备
int a210_ops_open(bio_dev *dev);

// 录入特征
int a210_ops_enroll(bio_dev *dev,
					OpsActions action,
					int uid,
					int bio_idx,
					char * bio_idx_name);

// 捕获特征
char * a210_ops_capture(bio_dev *dev, OpsActions action);

// 验证特征
int a210_ops_verify(bio_dev *dev, OpsActions action, int uid, int idx);

// 识别特征
int a210_ops_identify(bio_dev *dev,
					  OpsActions action,
					  int uid,
					  int idx_start,
					  int idx_end);

// 搜索特征
feature_info * a210_ops_search(bio_dev *dev,
							   OpsActions action,
							   int uid,
							   int idx_start,
							   int idx_end);

// 清理、删除特征
int a210_ops_clean(bio_dev *dev,
				   OpsActions action,
				   int uid,
				   int idx_start,
				   int idx_end);

// 获取特征列表
feature_info * a210_ops_get_feature_list(bio_dev *dev,
										 OpsActions action,
										 int uid,
										 int idx_start,
										 int idx_end);

// 设备插入回调函数
void a210_ops_attach(bio_dev *dev);

// 设备拔出回调函数
void a210_ops_detach(bio_dev *dev);

// 中断当前操作
int a210_ops_stop_by_user(bio_dev * dev, int waiting_ms);

// 关闭设备，Open的反向操作
void a210_ops_close(bio_dev *dev);

// 释放资源，Discover反向操作
void a210_ops_free(bio_dev *dev);

// 获取设备状态消息
const char * a210_ops_get_dev_status_mesg(bio_dev *dev);

// 获取操作状态消息
const char * a210_ops_get_ops_result_mesg(bio_dev *dev);

// 获取提示状态消息
const char * a210_ops_get_notify_mid_mesg(bio_dev *dev);

//// 内部函数，获取指静脉状态
//static int getFingerVeinData(bio_dev *dev,
//							 unsigned int sample_times,
//							 unsigned int interval);

//// 内部函数，查找指静脉
//feature_info * a210_fv_find(bio_dev *dev, int uid, int idx_start, int idx_end);

#if	defined(__cplusplus)
}
#endif	/* defined(__cplusplus) */

#endif // A210_H
