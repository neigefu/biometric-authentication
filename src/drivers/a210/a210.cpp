/*
 * Copyright (C) 2018 Tianjin KYLIN Information Technology Co., Ltd.
 *
 * Author: Droiing <jianglinxuan@kylinos.cn>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <biometric_common.h>
#include <biometric_stroge.h>
#include <biometric_intl.h>
#include <biometric_version.h>

#include "driver_ids.h"
#include "a210.h"

#include "liba210.h"

static bio_dev * a210_dev;
static int a210_enroll_id = -1;
static int a210_identify_id = -1;

char a210_notify_string[NOTIFY_STRING_MAX_LEN] = {0};

/******************** 内部函数 ********************/

void a210_set_ops_result_by_device_ops_ret(bio_dev * dev, int ops_type, int ret)
{
	switch (ret) {

	case -COM_OPS_RET_TIMEOUT:
//		bio_set_all_abs_status(dev,
//							   DEVS_COMM_IDLE,
//							   ops_type * 100 + OPS_COMM_TIMEOUT,
//							   ops_type * 100 + NOTIFY_COMM_TIMEOUT);
        bio_set_notify_abs_mid(dev, (ops_type * 100 + NOTIFY_COMM_TIMEOUT));
        bio_set_ops_abs_result(dev, (ops_type * 100 + OPS_COMM_TIMEOUT));
        bio_set_dev_status(dev, DEVS_COMM_IDLE);
		break;
	case -COM_OPS_RET_STOP_BY_USER:
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   ops_type * 100 + OPS_COMM_STOP_BY_USER,
							   ops_type * 100 + NOTIFY_COMM_STOP_BY_USER);
		A210_CancelOp();

#ifdef A210_DEBUG
		bio_print_debug(_("Hardware level cancel success, device status: %d\n"),
						dev->ops_result);
#endif
		break;
	case -COM_OPS_RET_FAIL_OR_NO_MATCH:
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   ops_type * 100 + OPS_COMM_NO_MATCH,
							   ops_type * 100 + NOTIFY_COMM_NO_MATCH);
		break;
	case -COM_OPS_RET_ERROR:
	default:
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   ops_type * 100 + OPS_COMM_ERROR,
							   A210_NOTIFY_INTERNAL_ERROR);
		break;

	}
}


void HandleSendInfo(char* cInfo, int ilen)
{
#ifdef A210_DEBUG_LOW_LEVEL
	if (NULL == cInfo)
	{
		return;
	}

	GString * print_list = g_string_new(NULL);
	g_string_append(print_list, _("A210SendInfo: "));

	int i = 0;
	for (i = 0; i < ilen; i++)
		g_string_append(print_list, "%02X ", cInfo[i]);

	bio_print_info("%s\n", print_list->str);
	g_string_free(print_list, TRUE);
#endif
}

int getIDFromResultString(char * str)
{
	char id_string[USER_ID_LEN] = {0};
//	int i;
	char * p;

	const char * CHAR_STRING = "ID: ";
	unsigned int len = strlen(CHAR_STRING);

	p = strstr(str, CHAR_STRING);
	if (p == NULL)
		return -1;

	if (strlen(p) > len)
		strcpy(id_string, p + len);

	id_string[USER_ID_LEN -1] = 0;
	if (strlen(id_string) > 0)
		return atoi(id_string);
	else
		return -1;
}

void parseResultString(char * str)
{
	/* 包含“识别成功”字符串代表需要处理识别结果 */
	if (strstr(str, _("Identify success")) != NULL)
	{
		a210_identify_id = getIDFromResultString(str);

#ifdef A210_DEBUG
		bio_print_info(_("Identified ID: %d\n"), a210_identify_id);
#endif
	}

	/* 包含“注册成功”字符串代表需要处理录入结果 */
	if (strstr(str, _("Enroll success")) != NULL)
	{
		a210_enroll_id = getIDFromResultString(str);

#ifdef A210_DEBUG
		bio_print_info(_("Enrolled ID: %d\n"), a210_enroll_id);
#endif
	}

	/* 其他操作成功无需解析字段 */
}

void HandleResult(char* cInfo, int ilen, int ops_ret)
{
	memset(a210_notify_string, 0, sizeof(a210_notify_string));

#ifdef A210_DEBUG
	bio_print_debug(_("A210OpsResult[%d]: %s\n"), ilen, cInfo);
#endif

	switch (ops_ret) {
	case COM_OPS_RET_RECV_CMD_DATA_EXTRACT_DOING:
		bio_set_notify_abs_mid(a210_dev, A210_NOTIFY_INTERNAL_NOTIFY);
		snprintf(a210_notify_string, sizeof(a210_notify_string), "%s", cInfo);
		break;
	case COM_OPS_RET_CHECKSUM_ERROR:
	case COM_OPS_RET_ERROR:
		bio_set_notify_abs_mid(a210_dev, A210_NOTIFY_INTERNAL_ERROR);
		snprintf(a210_notify_string, sizeof(a210_notify_string), "%s", cInfo);
		break;
	case COM_OPS_RET_SUCCESS:
		parseResultString(cInfo);
		break;
	case COM_OPS_RET_FAIL_OR_NO_MATCH:
		break;
	default:
		bio_set_notify_abs_mid(a210_dev, A210_NOTIFY_INTERNAL_ERROR);
		sprintf(a210_notify_string, _("Device returns unknown data and "
									  "skips processing"));
	}
}

void HandleRecvInfo(char* cInfo, int ilen)
{
#ifdef A210_DEBUG_LOW_LEVEL
	GString * print_list = g_string_new(NULL);
	g_string_append(print_list, _("A210RecvInfo: "));

	int i = 0;
	for (i = 0; i < ilen; i++)
		g_string_append(print_list, "%02X ", cInfo[i]);
	bio_print_info("%s\n", print_list->str);
	g_string_free(print_list, TRUE);
#endif
}

void HandleRecvData(char cCmd, unsigned char* cData, int nDatalen)
{
#ifdef A210_DEBUG_LOW_LEVEL
	GString * print_list = g_string_new(NULL);
	g_string_append(print_list, "当前命令: 0x%X, 返回数据：", cCmd);

	int i = 0;
	for (i = 0; i < nDatalen; i++)
		g_string_append(print_list, "%02X ", cData[i]);

	bio_print_info("%s\n", print_list->str);
	g_string_free(print_list, TRUE);
#endif
}

/******************** 外部函数 ********************/

int ops_configure(bio_dev * dev, GKeyFile * conf)
{
	dev->driver_id = A210_ID;
	dev->device_name = (char *)"a210";
    dev->full_name = (char *)(_("A210 iris recognition module"));
	dev->bioinfo.biotype = BioT_Iris;
	dev->bioinfo.stotype = StoT_OS;
	dev->bioinfo.eigtype = EigT_Eigenvalue;
	dev->bioinfo.vertype = VerT_Hardware;
	dev->bioinfo.idtype = IdT_Hardware;
	dev->bioinfo.bustype = BusT_Serial;
	dev->serial_info.fd = 0;

	dev->ops_configure = ops_configure;
	dev->ops_driver_init = a210_ops_driver_init;
	dev->ops_discover = a210_ops_discover;
	dev->ops_open = a210_ops_open;
	dev->ops_enroll = a210_ops_enroll;
	dev->ops_verify = a210_ops_verify;
	dev->ops_identify = a210_ops_identify;
	dev->ops_capture = a210_ops_capture;
	dev->ops_search = a210_ops_search;
	dev->ops_clean = a210_ops_clean;
//	dev->ops_get_feature_list = a210_ops_get_feature_list;
	dev->ops_get_feature_list = NULL;
	dev->ops_attach = NULL;
	dev->ops_detach = NULL;
	dev->ops_stop_by_user = a210_ops_stop_by_user;
	dev->ops_close = a210_ops_close;
	dev->ops_free = a210_ops_free;
	dev->ops_get_ops_result_mesg = a210_ops_get_ops_result_mesg;
	dev->ops_get_dev_status_mesg = a210_ops_get_dev_status_mesg;
	dev->ops_get_notify_mid_mesg = a210_ops_get_notify_mid_mesg;

	bio_set_drv_api_version(dev);
	bio_set_dev_status(dev, DEVS_COMM_IDLE);
	bio_set_ops_result(dev, OPS_COMM_SUCCESS);
	bio_set_notify_mid(dev, NOTIFY_COMM_IDLE);

	if (bio_dev_set_serial_path(dev, conf) != 0)
		return -1;

	dev->enable = bio_dev_is_enable(dev, conf);

	a210_dev = dev;
	return 0;
}

int a210_ops_driver_init(bio_dev *dev)
{
	// 关闭Debug日志功能
	A210_Startlog(1);
	A210_Init(HandleSendInfo, HandleRecvInfo, HandleResult, HandleRecvData);
	A210_SetTimeout(1);
	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	return 0;
}

// 驱动发现函数，返回发现的设备数量
int a210_ops_discover(bio_dev *dev)
{
	int ret = 0;

	// 设备探测阶段，设置超时时间为5s，防止进程长时间卡住
	A210_SetTimeout(5);
	ret = a210_ops_open(dev);
	a210_ops_close(dev);

	// 恢复超时时间
	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	if (ret < 0)
		return 0;
	else
		return 1;
}

// 打开设备
int a210_ops_open(bio_dev *dev)
{
	int ret = 0;

	bio_set_all_abs_status(dev,
						   DEVS_COMM_IDLE,
						   OPS_COMM_SUCCESS,
						   NOTIFY_COMM_IDLE);

	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		return -1;
	}

	bio_set_dev_status(dev, DEVS_OPEN_DOING);

	A210_SetTimeout(1);
	ret = A210_OpenComPort(dev->serial_info.path, 19200, 8, 1, 'N');
	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);
	if (ret < 0 )
	{
		/* A210_XXX的错误处理和状态变更交由wrapResultInfo函数处
		 * 理，wrapResultInfo函数由HandleResult实现 */
		a210_set_ops_result_by_device_ops_ret(dev, DEVS_OPEN_DOING / 100, ret);
		return -1;
	}

	ret = A210_StartDev();
	if (ret < 0)
	{
		a210_set_ops_result_by_device_ops_ret(dev, DEVS_OPEN_DOING / 100, ret);
		return -1;
	}

	bio_set_all_abs_status(dev,
						   DEVS_COMM_IDLE,
						   OPS_OPEN_SUCCESS,
						   NOTIFY_OPEN_SUCCESS);

	return 0;
}

// 录入特征
int a210_ops_enroll(bio_dev *dev,
					OpsActions action,
					int uid,
					int bio_idx,
					char * bio_idx_name)
{
	int ret = 0;
	int empty_sno = 0;
	int enroll_id = 0;
	char * sample_base64 = NULL;
	char a210_uid[USER_ID_LEN] = {0};

	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
		return -1;
	}

	bio_set_dev_status(dev, DEVS_ENROLL_DOING);

	/* 数据库中查询空闲采样编号 */
	empty_sno = bio_common_get_empty_sample_no(dev, A210_MIN_INDEX, A210_MAX_INDEX);

	if (empty_sno == -1)
	{
//		用户已满，无法存储，录入失败
		bio_set_dev_status(dev, DEVS_COMM_IDLE);
		bio_set_ops_result(dev, OPS_COMM_ERROR);
		bio_set_notify_abs_mid(dev, A210_NOTIFY_ENROLL_STORAGE_FULL);
		return -1;
	}

	/* 数据库中，用采样编号存储虹膜的用户ID */
#ifdef A210_DEBUG
	bio_print_debug("sno = %d\n", empty_sno);
#endif

	sprintf(a210_uid, "%d", empty_sno);
	ret = A210_DeleteTemplate(a210_uid);
//	if (ret < 0)
//	{
//		a210_set_ops_result_by_device_ops_ret(dev, DEVS_ENROLL_DOING / 100, ret);
//		return -1;
//	}

	bio_set_notify_abs_mid(dev, A210_NOTIFY_SEE_MIRROR);

	A210_SetTimeout(TIME_SAMPLE_TIMEOUT);
	a210_enroll_id = -1;
	ret = A210_Enroll(a210_uid);
	if (ret < 0)
	{
		a210_set_ops_result_by_device_ops_ret(dev, DEVS_ENROLL_DOING / 100, ret);
		return -1;
	}

	/* 获取录入的虹膜ID */
#ifdef A210_DEBUG
	bio_print_info(_("A210 enroll success, enroll_id = %d\n"), a210_enroll_id);
#endif

	if (a210_enroll_id >= 0)
		enroll_id = a210_enroll_id;
	a210_enroll_id = -1;

	/* 获取采样数据，进行base64编码 */
//	sprintf(a210_uid, "%d", enroll_id);
//	ret = A210_GetTemplate(a210_uid);
//	if (ret != COM_OPS_RET_SUCCESS)
//	{
//		bio_set_dev_status(dev, DEVS_COMM_IDLE);
//		bio_set_ops_result(dev, OPS_COMM_ERROR);
//		/* 内部错误由"HandleResult"函数和"RecvedCmdDataExtract"函数处理，
//		 * 此处无需设置notify状态 */
//		return -1;
//	}

	/* 操作数据库，关联uid和enroll_id，enroll存入采样编号中 */
	feature_info * info;
	feature_sample * sample;
	info = bio_sto_new_feature_info(uid,
									dev->bioinfo.biotype,
									dev->device_name,
									bio_idx,
									bio_idx_name);
	sample_base64 = bio_new_string((char *)"will get from device late.");
	sample = bio_sto_new_feature_sample(enroll_id, sample_base64);
	info->sample = sample;
	print_feature_info(info);

	sqlite3 *db = bio_sto_connect_db();
	bio_sto_set_feature_info(db, info);
	bio_sto_disconnect_db(db);

	bio_sto_free_feature_info(info);

	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	bio_set_all_abs_status(dev,
						   DEVS_COMM_IDLE,
						   OPS_ENROLL_SUCCESS,
						   NOTIFY_ENROLL_SUCCESS);

	return 0;
}

// 捕获特征
char * a210_ops_capture(bio_dev *dev, OpsActions action)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
		return NULL;
	}

	bio_set_dev_status(dev, DEVS_CAPTURE_DOING);

	A210_SetTimeout(TIME_SAMPLE_TIMEOUT);

	/* 未提供接口 */

	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	bio_set_all_abs_status(dev,
						   DEVS_COMM_IDLE,
						   OPS_CAPTURE_SUCCESS,
						   NOTIFY_CAPTURE_SUCCESS);
	return NULL;
}

// 验证特征
int a210_ops_verify(bio_dev *dev, OpsActions action, int uid, int idx)
{
	feature_info * info = NULL;
	int identify_id = -1;
	int ret = 0;

	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
		return -1;
	}

	bio_set_dev_status(dev, DEVS_VERIFY_DOING);
	bio_set_notify_abs_mid(dev, A210_NOTIFY_SEE_MIRROR);

	A210_SetTimeout(TIME_SAMPLE_TIMEOUT);
	a210_identify_id = -1;
	ret = A210_Identify(NULL);
	if (ret < 0)
	{
		a210_set_ops_result_by_device_ops_ret(dev, DEVS_VERIFY_DOING / 100, ret);
		return -1;
	}

	if (a210_identify_id >= 0)
	{
		identify_id = a210_identify_id;
		a210_identify_id = -1;
	}

	if (identify_id < 0)
	{
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   OPS_VERIFY_NO_MATCH,
							   NOTIFY_VERIFY_NO_MATCH);

		return -1;
	}

	sqlite3 *db = bio_sto_connect_db();
	info = bio_sto_get_feature_info(db,
									uid,
									dev->bioinfo.biotype,
									dev->device_name,
									idx,
									idx);
	bio_sto_disconnect_db(db);

	feature_sample * sample = NULL;
	if (info)
		sample = info->sample;

	int count = 0;
	while (sample != NULL)
	{
		if (identify_id == sample->no)
			count++;
		sample = sample->next;
	}
	bio_sto_free_feature_info(info);

	/* 不匹配 */
	if (count == 0)
	{
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   OPS_VERIFY_NO_MATCH,
							   NOTIFY_VERIFY_NO_MATCH);

		return -1;
	}

	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	bio_set_all_abs_status(dev,
						   DEVS_COMM_IDLE,
						   OPS_VERIFY_MATCH,
						   NOTIFY_VERIFY_MATCH);

	return 0;
}

// 识别特征
int a210_ops_identify(bio_dev *dev,
					  OpsActions action,
					  int uid,
					  int idx_start,
					  int idx_end)
{
	feature_info * info_list = NULL;
	int identify_id = -1;
	int ret = 0;

	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
		return -1;
	}

	bio_set_dev_status(dev, DEVS_IDENTIFY_DOING);
	bio_set_notify_abs_mid(dev, A210_NOTIFY_SEE_MIRROR);

	A210_SetTimeout(TIME_SAMPLE_TIMEOUT);
	a210_identify_id = -1;
	ret = A210_Identify(NULL);
	if (ret < 0)
	{
		a210_set_ops_result_by_device_ops_ret(dev, DEVS_IDENTIFY_DOING / 100, ret);
		return -1;
	}

	if (a210_identify_id >= 0)
	{
		identify_id = a210_identify_id;
		a210_identify_id = -1;
	}

	if (identify_id < 0)
	{
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   OPS_IDENTIFY_NO_MATCH,
							   NOTIFY_IDENTIFY_NO_MATCH);

		return -1;
	}

	sqlite3 *db = bio_sto_connect_db();
	info_list = bio_sto_get_feature_info(db,
									uid,
									dev->bioinfo.biotype,
									dev->device_name,
									idx_start,
									idx_end);
	bio_sto_disconnect_db(db);

	feature_info * info = info_list;
	int uid_ret = -1;
	while ((info != NULL) && (uid_ret == -1))
	{
		feature_sample * sample = info->sample;

		while (sample != NULL)
		{
			if (identify_id == sample->no)
			{
				uid_ret = info->uid;
				break;
			}
			sample = sample->next;
		}
		info = info->next;
	}
	bio_sto_free_feature_info(info);

	/* 未识别 */
	if (uid_ret == -1)
	{
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   OPS_IDENTIFY_NO_MATCH,
							   NOTIFY_IDENTIFY_NO_MATCH);

		return -1;
	}

	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	bio_set_all_abs_status(dev,
						   DEVS_COMM_IDLE,
						   OPS_IDENTIFY_MATCH,
						   NOTIFY_IDENTIFY_MATCH);

	return uid_ret;
}

// 搜索特征
feature_info * a210_ops_search(bio_dev *dev,
                               OpsActions action,
                               int uid,
                               int idx_start,
                               int idx_end)
{
    feature_info * info_list = NULL;
    int identify_id = -1;
    int ret = 0;

    if (dev->enable == FALSE) {
        bio_set_dev_status(dev, DEVS_COMM_DISABLE);
        bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
        return NULL;
    }

    bio_set_dev_status(dev, DEVS_SEARCH_DOING);
    bio_set_notify_abs_mid(dev, A210_NOTIFY_SEE_MIRROR);

    A210_SetTimeout(TIME_SAMPLE_TIMEOUT);
    a210_identify_id = -1;
    ret = A210_Identify(NULL);
    if (ret < 0)
    {
        a210_set_ops_result_by_device_ops_ret(dev, DEVS_SEARCH_DOING / 100, ret);
        bio_print_info(_("a210_ops_search return NULL, device status: %d\n"),
                       dev->ops_result);
        return NULL;
    }

    if (a210_identify_id >= 0)
    {
        identify_id = a210_identify_id;
        a210_identify_id = -1;
    }

    if (identify_id < 0)
    {
        bio_set_all_abs_status(dev,
                               DEVS_COMM_IDLE,
                               OPS_SEARCH_NO_MATCH,
                               NOTIFY_SEARCH_NO_MATCH);

        return NULL;
    }

    sqlite3 *db = bio_sto_connect_db();
    info_list = bio_sto_get_feature_info(db,
                                    uid,
                                    dev->bioinfo.biotype,
                                    dev->device_name,
                                    idx_start,
                                    idx_end);
    bio_sto_disconnect_db(db);

    feature_info * info = info_list;
    feature_info found;
    feature_info *found_list = NULL;
    found.next = NULL;
    found_list = &found;
    while(info != NULL)
    {
        feature_sample * sample = info->sample;
        while (sample != NULL)
        {
            if (identify_id == sample->no)
            {
                found_list->next = bio_sto_new_feature_info(info->uid, info->biotype,
                                                 info->driver, info->index,
                                                 info->index_name);
                found_list->next->sample = bio_sto_new_feature_sample(sample->no, sample->data);
                found_list = found_list->next;
                break;
            }
            sample = sample->next;
        }
        info = info->next;
    }
    found_list = found.next;
    bio_sto_free_feature_info(info);

    /* 未找到到 */
    if (found_list == NULL)
    {
        bio_set_all_abs_status(dev,
                               DEVS_COMM_IDLE,
                               OPS_SEARCH_NO_MATCH,
                               OPS_SEARCH_NO_MATCH);
        return NULL;
    }

    A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

    bio_set_all_abs_status(dev,
                           DEVS_COMM_IDLE,
                           OPS_SEARCH_MATCH,
                           NOTIFY_SEARCH_MATCH);

    return found_list;
}

// 清理、删除特征
int a210_ops_clean(bio_dev *dev,
				   OpsActions action,
				   int uid,
				   int idx_start,
				   int idx_end)
{
	feature_info * flist_full = NULL;
	feature_info * flist_will_clean = NULL;
	int ret = 0;

	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
		return -1;
	}

	bio_set_dev_status(dev, DEVS_CLEAN_DOING);

	sqlite3 *db = bio_sto_connect_db();
	flist_full = bio_sto_get_feature_info(db,
										  -1,
										  dev->bioinfo.biotype,
										  dev->device_name,
										  0,
										  -1);
	flist_will_clean = bio_sto_get_feature_info(db,
												uid,
												dev->bioinfo.biotype,
												dev->device_name,
												idx_start,
												idx_end);
	ret = bio_sto_clean_feature_info(db,
									 uid,
									 dev->bioinfo.biotype,
									 dev->device_name,
									 idx_start,
									 idx_end);
	bio_sto_disconnect_db(db);

	if (ret < 0) {
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   OPS_CLEAN_ERROR,
							   OPS_CLEAN_ERROR);
		return ret;
	}

	int a210_id_used[A210_MAX_INDEX];
	memset(a210_id_used, 0, sizeof(a210_id_used));

	feature_info * info;
	info = flist_full;
	while (info != NULL)
	{
		feature_sample * sample = info->sample;
		while (sample != NULL)
		{
			a210_id_used[sample->no]++;
			sample = sample->next;
		}
		info = info->next;
	}

	info = flist_will_clean;
	while (info != NULL)
	{
		feature_sample * sample = info->sample;
		while (sample != NULL)
		{
			a210_id_used[sample->no]--;
			if (a210_id_used[sample->no] <= 0)
			{
				char del_id_str[USER_ID_LEN] = {0};
				sprintf(del_id_str, "%d", sample->no);

#ifdef A210_DEBUG
				bio_print_debug("del sno: %d\n", sample->no);
#endif
				/*
				 * 删除设备中ID出现的异常不做处理，下次录入会再次删除，
				 * 此时再次出错则处理
				 */
				A210_DeleteTemplate(del_id_str);
			}
			sample = sample->next;
		}
		info = info->next;
	}

	bio_sto_free_feature_info_list(flist_full);
	bio_sto_free_feature_info_list(flist_will_clean);

	if (ret != COM_OPS_RET_SUCCESS)
	{
		bio_set_all_abs_status(dev,
							   DEVS_COMM_IDLE,
							   OPS_CLEAN_ERROR,
							   OPS_CLEAN_ERROR);

		return -1;
	}

	bio_set_all_abs_status(dev,
						   DEVS_COMM_IDLE,
						   OPS_CLEAN_SUCCESS,
						   OPS_CLEAN_SUCCESS);

	return 0;
}

// 获取特征列表
feature_info * a210_ops_get_feature_list(bio_dev *dev,
										 OpsActions action,
										 int uid,
										 int idx_start,
										 int idx_end)
{
	if (dev->enable == FALSE) {
		bio_set_dev_status(dev, DEVS_COMM_DISABLE);
		bio_set_notify_mid(dev, NOTIFY_COMM_DISABLE);
		return NULL;
	}

	bio_set_dev_status(dev, DEVS_GET_FLIST_DOING);

	feature_info * finfo_list = NULL;
	sqlite3 *db = bio_sto_connect_db();
	finfo_list = bio_sto_get_feature_info(db, uid, dev->bioinfo.biotype,
												 dev->device_name, idx_start,
												 idx_end);
	print_feature_info(finfo_list);
	bio_sto_disconnect_db(db);

	bio_set_all_abs_status(dev,DEVS_COMM_IDLE,
						   OPS_GET_FLIST_SUCCESS,
						   NOTIFY_GET_FLIST_SUCCESS);

	return finfo_list;
}

// 中断当前操作
int a210_ops_stop_by_user(bio_dev * dev, int waiting_ms)
{
#ifdef A210_DEBUG
	bio_print_info(_("Device %s[%d] received interrupt request\n"),
				   dev->device_name, dev->driver_id);
#endif

	int timeout = bio_get_ops_timeout_ms();
	int timeused = 0;
	int dev_status = bio_get_dev_status(dev);
	int ops_type = dev_status / 100;

	if (waiting_ms < timeout)
		timeout = waiting_ms;

	if (bio_get_dev_status(dev) % 100 != DEVS_COMM_IDLE)
	{
		bio_set_dev_status(dev, ops_type * 100 + DEVS_COMM_STOP_BY_USER);

		A210_SetStopFlag(true);

		while ((bio_get_dev_status(dev) % 100 != DEVS_COMM_IDLE) ||
			   A210_GetStopFlag())
		{
			if (timeused > timeout)
				break;

			timeused += TIME_INTERVAL;
			usleep(TIME_INTERVAL * 1000);
		}
	}
#ifdef A210_DEBUG
	bio_print_info(_("software level cancel success, dev_status = %d, StopFlag = %d, "
					 "timeused = %d, timeout = %d\n"),
		   bio_get_dev_status(dev) % 100,
		   A210_GetStopFlag(),
		   timeused,
		   timeout);
#endif

	if (bio_get_dev_status(dev) % 100 == DEVS_COMM_IDLE)
		return 0;

	// 中断失败，还原操作状态
	bio_print_error("Stop failed to restore operation status\n");
	bio_set_dev_status(dev, dev_status);
	return -1;
}

// 关闭设备，Open的反向操作
void a210_ops_close(bio_dev *dev)
{
	A210_SetTimeout(1);
	A210_CloseComPort();
	A210_SetTimeout(bio_get_ops_timeout_ms() / 1000);

	bio_set_dev_status(dev, DEVS_COMM_IDLE);
}

// 释放资源，Discover反向操作
void a210_ops_free(bio_dev *dev)
{
	A210_ClearLog();
}

// 获取设备状态消息
const char * a210_ops_get_dev_status_mesg(bio_dev *dev)
{
	return NULL;
}

// 获取操作状态消息
const char * a210_ops_get_ops_result_mesg(bio_dev *dev)
{
	return NULL;
}

// 获取提示状态消息
const char * a210_ops_get_notify_mid_mesg(bio_dev *dev)
{
	switch (bio_get_notify_mid(dev))
	{
	case A210_NOTIFY_INTERNAL_NOTIFY:
	case A210_NOTIFY_INTERNAL_ERROR:
		return a210_notify_string;
	case A210_NOTIFY_SEE_MIRROR:
		return _("Look at the mirror to the iris device");
	case A210_NOTIFY_ENROLL_STORAGE_FULL:
		return "There's not enough space on the device, unable to store user "
			   "characteristics, enroll failed";

	default:
		return NULL;
	}
}
